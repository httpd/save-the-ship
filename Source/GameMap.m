
//
//  GameMap.m
//  Save the ship!
//
//  Created by Piotrek on 18.04.2013.
//
//

#import "GameMap.h"
#import "MainGameStage.h"
#define PAGING_MARGIN 8
@implementation GameMap
- (id)init
{
    self = [super init];
    if (self) {
        map = [SPSprite sprite];
        SPQuad *background = [SPImage imageWithContentsOfFile:@"mapa.png"];
        background.width = [Sparrow stage].width*3;
        [map addChild:background];
        [self addChild:map];
        [map addEventListener:@selector(onTouch:) atObject:self forType:SP_EVENT_TYPE_TOUCH];
        
        SPButton *back = [SPButton buttonWithUpState:[SPTexture textureWithContentsOfFile:@"start.png"] text:@"Back to menu"];
        back.x = 50;
        back.y = 50;
        [back addEventListener:@selector(backClicked:) atObject:self forType:SP_EVENT_TYPE_TRIGGERED];
        [self addChild:back];
        for (int i=0; i<10; i++) {
            SPButton *button = [SPButton buttonWithUpState:[SPTexture textureWithContentsOfFile:@"lev.png"]];
            button.y = 200;
            button.x = 50+40*i;
            button.scaleX=0.8;
            button.scaleY=0.8;
            button.name = [NSNumber numberWithInt:i].stringValue;
            [map addChild:button];
            [button addEventListener:@selector(onLevelSelect:) atObject:self forType:SP_EVENT_TYPE_TRIGGERED];
        }
        

    }
    return self;
}
- (void)onLevelSelect: (SPEvent *)event
{
//    NSLog(@"%d", ((SPButton *)event.target).name.intValue);
    [self.rootGameStage startLevel:((SPButton *)event.target).name.intValue];
}
- (void)onTouch: (SPTouchEvent *)event
{
    NSSet *set = [event touchesWithTarget:map andPhase:SPTouchPhaseBegan];
    if (set.count>0) {
        SPTouch *touch = [set anyObject];
        SPPoint *touchPoint  = [touch locationInSpace:map];
        screenTouched = touchPoint.x/[Sparrow stage].width;
    }
    set = [event touchesWithTarget:map andPhase:SPTouchPhaseMoved];
    if (set.count>0) {
            SPTouch *touch = [set anyObject];
        float finalx = map.x + [touch movementInSpace:self].x;
        if (finalx>0) {
            map.x = map.x +[touch movementInSpace:self].x*0.5;
        }else if(finalx<-map.width+[Sparrow stage].width)
        {
            map.x = map.x +[touch movementInSpace:self].x*0.5;
        }else
        {
            map.x = finalx;
        }
    }
    set = [event touchesWithTarget:map andPhase:SPTouchPhaseEnded];
    if (set.count>0) {
        if (map.x>0) {
            SPTween *tween = [SPTween tweenWithTarget:map time:0.5 transition:SP_TRANSITION_EASE_OUT];
            [tween animateProperty:@"x" targetValue:0];
            [[Sparrow juggler] addObject:tween];
        }else if(map.x<-map.width+[Sparrow stage].width)
        {
            SPTween *tween = [SPTween tweenWithTarget:map time:0.5 transition:SP_TRANSITION_EASE_OUT];
            [tween animateProperty:@"x" targetValue:-map.width+[Sparrow stage].width];
            [[Sparrow juggler] addObject:tween];

        }else
        {
            SPTouch *touch = [set anyObject];
            float vector = [touch movementInSpace:self].x;
            int kierunek = (vector<0)?-1:1;
            if (vector<0) {
                vector*=-1;
            }
            float px = -map.x+[Sparrow stage].width/2;
            int sekcja = px/[Sparrow stage].width;
//            NSLog(@"%f %d %f, %d, %d",px, sekcja, vector, kierunek, screenTouched);
            float target;
            if (vector<PAGING_MARGIN) {
                target = -sekcja*[Sparrow stage].width;
            }else
            {
                target= (-screenTouched+kierunek)*[Sparrow stage].width;
            }
//            NSLog(@"target; %f", target);
            if (target>0) {
                target=0;
            }
            [[Sparrow juggler] removeObjectsWithTarget:map];
            SPTween *tween = [SPTween tweenWithTarget:map time:0.5 transition:SP_TRANSITION_EASE_OUT];
            [tween animateProperty:@"x" targetValue:target];
            [[Sparrow juggler] addObject:tween];

        }
    
    }

}
- (void)backClicked: (SPEvent *)event
{        NSLog(@"back to menu");
    [self.rootGameStage backToMenu];
}
@end
