//
//  MalaFala.h
//  Save the ship!
//
//  Created by Piotrek on 21.05.2013.
//
//

#import <Foundation/Foundation.h>
enum FalaType
{
    FalaWybuch,
    FalaSkrzynka
};
@interface MalaFala : SPEventDispatcher <SPAnimatable>
{
    float przesuniecie;
    float max;
    int prog;
}
- (float)wysokoscDlaX: (int)x iReszty: (float)reszta;

@property (nonatomic) enum FalaType typ;
@property (nonatomic) float pozycja;
@property (nonatomic) float wysokoscFalki;
@property (nonatomic) float dlugosc;
@property (nonatomic) int szybkosc;
@end
