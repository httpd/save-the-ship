//
//  GunpowerBarrel.m
//  Save the ship!
//
//  Created by Piotrek on 17.01.2013.
//
//

#import "GunpowerBarrel.h"
#import "CargoBox.h"
#import "SXParticleSystem.h"
#import "Game.h"
@implementation GunpowerBarrel
- (id)initWithWorld: (b2World *)_world andGame:(Game *)g
{
    self = [super initWithWorld:_world];
    if (self) {
        self.game = g;
        blocks = [NSMutableArray array];
        SPImage *quad = [SPImage imageWithContentsOfFile:@"beczka.png"];
        quad.pivotX = quad.width/2;
        quad.pivotY = quad.height/2;
        [self addChild:quad];

        b2BodyDef bodyDef;
        bodyDef.position.Set(720.0/PTM_RATIO, (220.0)/PTM_RATIO);
        bodyDef.type = b2_dynamicBody;
        
        bodyDef.userData = (__bridge void *)self;
        body = world->CreateBody(&bodyDef);
        
        b2PolygonShape box;
        box.SetAsBox(quad.width/2/PTM_RATIO, quad.height/2/PTM_RATIO);
        
        b2FixtureDef fixtureDef;
        fixtureDef.shape = &box;
        fixtureDef.density =  1.0f;
        fixtureDef.friction = 0.3f;
        
        body->CreateFixture(&fixtureDef);
        
        zasieg = 20.0f;
        sila = 400.0f;
        
        self.system = [SXParticleSystem particleSystemWithContentsOfFile:@"lont.pex"];
        self.system.y-=40;

    }
    return self;
}
- (void)colisionWithShip
{
//    [self removeFromParent];
    if (!self.destroyed) {
        for (FloatingBody *fBody in self.floatingBodies) {
            if ([fBody isKindOfClass:[CargoBox class]]) {
                float odleglosc = [SPPoint distanceFromPoint:[SPPoint pointWithX:fBody.x y:fBody.y] toPoint:[SPPoint pointWithX:self.x y:self.y]];
                
                if (odleglosc<zasieg) {
                    odleglosc = 1;
                }else
                {
                    odleglosc = pow((float)(odleglosc/zasieg), -0.2f);
                }
                
//                NSLog(@"odleglosc %f, %@", odleglosc, fBody);
                
                //            vector = b2Vec2(0,
                //                            600*fBody->body->GetMass()*odleglosc);
                [self  registerCallback:^(void){
                    body->SetActive(NO);
                    b2Vec2 vector;
                    vector = b2Vec2((arc4random()%60-30.f)/100.f*odleglosc*fBody->body->GetMass(),
                                    sila*odleglosc*fBody->body->GetMass());
                    NSLog(@"%f", vector.y);
                    fBody->body->ApplyForce(vector, fBody->body->GetWorldCenter());
                }];
                
                //            fBody->body->ApplyLinearImpulse(vector, fBody->body->GetWorldCenter());
            }
        }
        NSLog(@"bum!");
        
        SPSound *sound = [SPSound soundWithContentsOfFile:@"Wybuch.mp3"];
        self.channel = [sound createChannel];
        [self.channel play];
        self.channel.loop = NO;
        
        [self.game.fala addWaveAtPoint:self.x wysokosc:40 dlugosc:240 andSzybkosc:400];
        // create particle system
        [self.game explosionOnPoint:[SPPoint pointWithX:self.x y:self.y]];
        self.destroyed = YES;
        self.alpha = 0;
//        world->DestroyBody(body);
//        [self.floatingBodies removeObject:self];

    }

}
- (void)registerCallback:(void(^)())callback {
    [blocks addObject:[callback copy]];
}

- (void)executePhysics {
    for (void (^block)() in blocks) {
        block();
        
    }
    [blocks removeAllObjects];
}
@end
