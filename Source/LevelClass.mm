//
//  LevelClass.m
//  Save the ship!
//
//  Created by Piotrek on 20.01.2013.
//
//

#import "LevelClass.h"
#import "Game.h"
#import "Level.h"
#import "LevelParser.h"
#define FALA_EPS 40
@implementation BlokFali
@synthesize koniecBloku, dlugoscBloku, typ;
- (void)setDlugoscBloku:(int)_dlugoscBloku
{
    if(_dlugoscBloku<FALA_EPS)
    {
        _dlugoscBloku = FALA_EPS+_dlugoscBloku;
    }
dlugoscBloku = _dlugoscBloku;
}
- (NSString *)description {return self.debugDescription; }
- (NSString *)debugDescription
{
    return [NSString stringWithFormat:@"BlokFali %@ dl: %d wys %d typ %d", super.description, self.dlugoscBloku, self.koniecBloku, self.typ];
}
@end

@implementation LevelClass
@synthesize dlugosc, predkoscFalowania, poziomMorza, gameWidth;
- (id)init
{
    self = [super init];
    if (self) {
        
        dlugosc = 0;
        predkoscFalowania = 0;
        poziomMorza = 170.0f;
        gameWidth = 0;
        self.iloscBomb = 0;
        self.iloscSkrzyn = 0;
        self.continues = YES;
        self.numerFali = 0;
        
        self.gameWidth = 3000;
        self.continues = NO;
        
        self.iloscSkrzyn=4;
        self.numerFali = 0;
        self.bombs = new CGPoint[30];
        self.fale = [NSArray array];
        self.predkoscFalowania  = 6;
        self.predkoscPlywania = 150;
        self.pakietyPozycje = [NSArray array];
        self.runway = 5;
    }
    return self;
}
- (void)configure
{
    self.petla = self.fale.count;

}
- (void)startLevel
{
    
}
- (BlokFali *)generujElement
{
    return nil;
}
+ (LevelClass *)levelClassForGameMode:(enum gameMode)mode andLevel:(int)lev
{
    LevelClass *level;
    if (mode==gameModeTest) {
        level = [LevelParser loadLevelsForChapter:0 andLevel:0];
    }if(mode==gameModeSurvival)
    {
        level = [LevelParser loadLevelsForChapter:1 andLevel:1];

    }if (mode==gameModeLevel) {
        level = [LevelParser loadLevelsForChapter:0 andLevel:lev];
    }
    return level;
}
- (void)wywalonaSkrzynia
{
    self.iloscSkrzyn--;

}
- (void)zeroSpeed
{
    
}
@end
