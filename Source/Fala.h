//
//  Fala.h
//  Save the ship!
//
//  Created by Piotrek Knapczyk on 30.05.2012.
//  Copyright (c) 2012 IdeaStudio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Box2D.h"
#import "Statek.h"
#import "CargoBox.h"
#import "FalaRender.h"
#import "Piana.h"
#import "LevelClass.h"
#import "MalaFala.h"
#import "SXParticleSystem.h"

@class Game;

@interface Fala : SPDisplayObjectContainer <SXParticleSystemDelegate>
{
    NSMutableArray *tablicaFizyczna;
    b2Body *physBody;
    CGPoint th[SECTIONS];
    int renderTableCount;
//    FalaRender *falaRender;
//    FalaRender *falaRender2;

    int przesuniecie;
    
    Piana *pianaRender;
    int renderLenght;
    
    //    CGPoint renderTable[880];
    
    int alfa;
    //    int tableLenght;
    //    CGPoint *tablica;
    
    int koniecWygenerowanejSekcji;
    int poczatekWygenerowanejSekcji;
    
    int poczatkowaWysokosc;
    
    int rozdzielczoscGrafiki;
    
    b2Vec2 normal;
	/// The height of the fluid surface along the normal
	double fluidOffset;
	/// The fluid density
	float32 density;
	/// Fluid velocity, for drag calculations
	b2Vec2 velocity;
	/// Linear drag co-efficient
	float32 linearDrag;
	/// Linear drag co-efficient
	float32 angularDrag;
	/// If false, bodies are assumed to be uniformly dense, otherwise use the shapes densities
    
    NSMutableArray *falki;
    int indexFali;
    int iloscFali;
    
}

- (void)step;
- (void)stepWithBodies: (NSArray *)bodies;
- (void)stepWithBody: (b2Body *)body;

- (void)configure;
- (id)initWithWidth:(float)width height:(float)height ;

- (void)addWaveAtPoint: (int)point wysokosc: (int)wysokosc dlugosc: (int)dlugosc andSzybkosc: (int)szybkosc;

@property (nonatomic, weak) Game *game;


@property (nonatomic, strong) NSMutableArray *tablicaDanych;
@property (nonatomic) long long *offset;
@property (nonatomic) double frameRate;

//@property (nonatomic, unsafe_unretained) SXParticleSystem *explosionEmiter;
@property (nonatomic, assign) bool validate;
@property (nonatomic) int pozycja;
@property (nonatomic) float wysokoscFalki;
@property (nonatomic, strong) FalaRender *falaRender;
@property (nonatomic, strong) FalaRender *falaMask;

@end
