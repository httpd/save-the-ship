//
//  Menu.h
//  Save the ship!
//
//  Created by Piotrek on 17.09.2012.
//
//

#import <Foundation/Foundation.h>
@class MainGameStage;
@interface Menu : SPDisplayObjectContainer
{
}
@property (nonatomic, unsafe_unretained) MainGameStage *rootGameStage;
- (void)setup;
@end
