//
//  FirstLevel.m
//  Save the ship!
//
//  Created by Piotrek on 05.06.2013.
//
//

#import "Level.h"
#import "Game.h"
#import "MainGameStage.h"
@implementation Level
@synthesize skrzynkiZKolem = _skrzynkiZKolem;
- (id)init
{
    self = [super init];
    if (self) {
        alfa = 0;
    }
    return self;
}
- (void)dodajPakietMonetWX: (float)x y:(float)y
{
//    NSLog(@"Pakiet x: %f y: %f w: %d h:%d", x, y, self.PakietIloscW, self.PakietIloscH);
    self.pakietyPozycje = [self.pakietyPozycje arrayByAddingObject:[SPPoint pointWithX:x y:y]];
    
}
- (void)configure
{
    [super configure];
    self.skrzynkiZKolem = 0;
    if (self.levelNumber==0) {
//        self.petla = 2;
//        self.skrzynkiZKolem = self.iloscSkrzyn;
    }

}
- (void)setSkrzynkiZKolem:(int)skrzynkiZKolem
{
    NSLog(@"%d", skrzynkiZKolem);
    _skrzynkiZKolem = skrzynkiZKolem;
    if (skrzynkiZKolem==0) {
        self.petla = self.fale.count;
        [self startSailing];
    }
}
- (void)startLevel
{
    [super startLevel];
    if (self.levelNumber==0) {
        InfoBox *help = [[InfoBox alloc] init];
        help.title.text = @"Title";
        help.text.text = @"Move boxes on the ship by swipeing";
        
        [self.game.gameContainer addChild:help];
    }
//    if (self.levelNumber!=0) {
        [self startSailing];
//    }


}
- (void)startSailing
{
    self.game.statek.szybkoscStatku = 0; //170
    SPTween *tween = [SPTween tweenWithTarget:self.game.statek time:self.runway transition:SP_TRANSITION_EASE_IN_OUT];
    [tween animateProperty:@"szybkoscStatku" targetValue:self.predkoscPlywania];
    [self.game.juggler addObject:tween]; // MARK: Comment to stop sailing
    

}
- (BlokFali *)generujElement;
{
    self.numerFali = self.numerFali%self.fale.count;
    BlokFali *blok  = [self.fale objectAtIndex:self.numerFali]; self.numerFali++;
    NSLog(@"s %@", blok);
    return blok;
}
- (void)wywalonaSkrzynia
{
    [super wywalonaSkrzynia];
    if (self.iloscSkrzyn==0) {
//        self.gameWidth = 0;
//        self.continues = NO;
        [self.game gameOver];
        
    }
    NSLog(@"Boxes left %d", self.iloscSkrzyn);
}
- (void)zeroSpeed
{
//    self.petla = 2;
//    [self.game.rootGameStage startLevel:0];
//    [self.game.fala removeFromParent];
}
@end
