//
//  Fala.m
//  Save the ship!
//
//  Created by Piotrek Knapczyk on 30.05.2012.
//  Copyright (c) 2012 IdeaStudio. All rights reserved.
//

#import "Fala.h"
#include <math.h>
#include <vector>
#include "Game.h"
@implementation Fala
@synthesize offset = _offset ,frameRate, tablicaDanych = _tablicaDanych, validate;
#pragma mark - Syntesize

- (void)setOffset:(long long *)offset
{
    _offset = offset;
    if (self.falaRender!=nil) {
        if (self.falaRender.offset==nil) {
            self.falaRender.offset = new long long;
            
            self.falaRender.offset = offset;
            self.falaMask.offset = self.falaRender.offset;
        }
        if (pianaRender.offset==nil) {
//            pianaRender.offset = new int;
            
            pianaRender.offset = offset;
            
        }
    }
    
}
- (NSMutableArray *)tablicaDanych
{
    if (_tablicaDanych == nil) {
        _tablicaDanych = [[NSMutableArray alloc]init];
    }
    return _tablicaDanych;
}
#pragma mark - Inicjalizacja
- (id)initWithWidth:(float)width height:(float)height
{
    self = [super init];
    if (self) {
        validate = NO;
        //        [self generateTestData];
        poczatekWygenerowanejSekcji = 0;
        koniecWygenerowanejSekcji = 0;
        //        th = new CGPoint[kMaxLenght*2];
        przesuniecie  = 0;
        renderLenght = 110;
        //        alfa = 1;
        
        normal.Set(0.0f, 1.0f);
        fluidOffset = 200.0f/PTM_RATIO;
        density = 2.0f;
        linearDrag = 5.0f;
        angularDrag = 10.0f;
        self.pozycja = 750;
        self.wysokoscFalki = 20;
        falki = [[NSMutableArray alloc]init];

        indexFali = 0;
        iloscFali = 0;
        
    }
    return self;
}
- (void)configure
{
    [self generujTabliceDlaPoziomu:1 zRozdzielczoscia:10 zCzasem:1/30.0f];
    tablicaFizyczna = [[NSMutableArray alloc] init];

    
    self.falaRender = [[FalaRender alloc] initWithLevel:self.game.level.levelNumber];
    [self.falaRender setup];
    self.falaRender.alpha = 0.3f;
    
    self.falaRender.lenght = renderTableCount;
//    [self addChild:self.falaRender];
    
    self.falaMask = [[FalaRender alloc] initWithLevel:self.game.level.levelNumber];
//    self.falaMask.alpha = 1;
    self.falaMask.lenght = renderTableCount;
    self.falaMask.vertexData = self.falaRender.vertexData;
    self.falaMask.baseEffect = self.falaRender.baseEffect;
    
    self.falaMask.vertexBufferName = self.falaRender.vertexBufferName;
//    [self addChild:self.falaRender2];

    [self addEventListener:@selector(onEnterFrame:) atObject:self forType:SP_EVENT_TYPE_ENTER_FRAME];
}
- (void)addWaveAtPoint: (int)point wysokosc: (int)wysokosc dlugosc: (int)dlugosc andSzybkosc: (int)szybkosc
{
    MalaFala *fala = [[MalaFala alloc]init];
    fala.pozycja = point+[Sparrow stage].width;
    fala.wysokoscFalki = wysokosc;
    fala.dlugosc = dlugosc;
    fala.szybkosc = szybkosc;

    [self.game.juggler addObject:fala];
    fala.typ = FalaWybuch;
    [falki addObject:fala];
}
#pragma mark - Delegate
- (bool)canShowParticleInPoint:(SPPoint *)point
{
//    if (point.y<100) {
//        return NO;
//    }
//    return YES;

    float ey = [self thForX:point.x];
    if (ey>=point.y) {
        return NO;
    }else
    {
        return YES;
    }
}

#pragma mark - Krok
- (void)generujTabliceDlaPoziomu: (int)poziom
                zRozdzielczoscia: (int)rozdzielczosc
                         zCzasem: (float)passedTime

{
    if (validate) {
        przesuniecie+=self.game.level.predkoscFalowania*(passedTime/(1.0f/Sparrow.currentController.framesPerSecond));
    }
    rozdzielczoscGrafiki = rozdzielczosc;
    int begin;
    if (self.offset==nil) {
        begin = 0;
    }else
        begin = -*self.offset;
    
    int przesuniecieLokalne = przesuniecie;
    przesuniecieLokalne/=rozdzielczosc;
    przesuniecieLokalne*=rozdzielczosc;
    double reszta = przesuniecie-przesuniecieLokalne;
    reszta/=rozdzielczosc;
    //
    //    vector<int>etablica;
    
    if (iloscFali==0) {
        iloscFali++;
        NSNumber *b = [NSNumber numberWithInt: self.game.level.numerFali++%self.game.level.petla];
        [self.tablicaDanych addObject:b];
        BlokFali *fala = [self.game.level.fale objectAtIndex:(indexFali)%self.game.level.petla];
        koniecWygenerowanejSekcji +=  fala.dlugoscBloku;
        iloscFali++;
    }
    
    while(koniecWygenerowanejSekcji<begin+3*[Sparrow stage].width+przesuniecie) {
        //        NSLog(@"dodaj obiekt");
        //        NSLog(@"koniec %d, begin %f", koniecWygenerowanejSekcji, begin+ewidth);
        
//        BlokFali *b = [self.game.level generujElement];
//        [self.tablicaDanych addObject:b];
//        koniecWygenerowanejSekcji += b.dlugoscBloku;
//        NSNumber *b = [NSNumber numberWithInt: self.game.level.numerFali++%self.game.level.fale.count];
//        [self.tablicaDanych addObject:b];
        BlokFali *fala = [self.game.level.fale objectAtIndex:(indexFali+iloscFali)%self.game.level.petla];
        koniecWygenerowanejSekcji +=  fala.dlugoscBloku;
        iloscFali++;
    }
//    NSNumber *ostatni = [self.tablicaDanych objectAtIndex:0];
    BlokFali *B = [self.game.level.fale objectAtIndex:indexFali];
//    NSLog(@"Blok B %@", B);
    while(poczatekWygenerowanejSekcji+B.dlugoscBloku+10<begin+przesuniecie) {
        //        NSLog(@"Usun obiekt");
        poczatekWygenerowanejSekcji+=B.dlugoscBloku;
        poczatkowaWysokosc = B.koniecBloku;
//        [self.tablicaDanych removeObject:ostatni];
        indexFali = (indexFali+1)%self.game.level.petla;
        iloscFali--;
        B = [self.game.level.fale objectAtIndex:indexFali];
    }
    float lastx=0;
    float lasty=poczatkowaWysokosc;
    int roundedOffset = begin/rozdzielczosc*rozdzielczosc;
    
    int e=0;
    int h=0;
    float wysokosc=0;

    for (int i=0; i<iloscFali; i++) {
        BlokFali *B = [self.game.level.fale objectAtIndex:(i+indexFali)%self.game.level.petla];
        for(int i=0; i<(float)B.dlugoscBloku/rozdzielczosc; i++)
        {
            if (B.typ==falaA) {
                wysokosc = ( ((cos(((float)i+reszta)/(B.dlugoscBloku/rozdzielczosc)*M_PI)-1)/2)*(lasty-B.koniecBloku)+lasty);
            }else if (B.typ==falaB)
            {
                wysokosc =  (-sin(((float)i+reszta)/(B.dlugoscBloku/rozdzielczosc)*M_PI_2))*(lasty-B.koniecBloku)+lasty;
                
            }else
            {
                float ratio = ((float)i+reszta)/(B.dlugoscBloku/rozdzielczosc);
                ratio = ratio*ratio*ratio;
                wysokosc = -ratio*(lasty-B.koniecBloku)+lasty;

            }
            float x=0;
            
//            float dlugosc = 50.0f;
//            if (rozdzielczosc*h+roundedOffset >= self.pozycja && rozdzielczosc*h+roundedOffset<=self.pozycja+dlugosc) {
//                int numer = rozdzielczosc*h+roundedOffset-self.pozycja;
//
//                x+=self.wysokoscFalki*sin((float(numer))/(dlugosc)*2*M_PI);
//            }
            NSArray *scheduledForDeletion = [NSArray array];

            for (MalaFala *fala in falki) {
                x+=[fala wysokoscDlaX:rozdzielczosc*h+roundedOffset iReszty:reszta];
                if (![self.game.juggler containsObject:fala]) {
                    scheduledForDeletion = [scheduledForDeletion arrayByAddingObject:fala];
                }
            }
            [falki removeObjectsInArray:scheduledForDeletion];
            
            
            if (
                e>(roundedOffset-poczatekWygenerowanejSekcji)/rozdzielczosc+(int)przesuniecie/rozdzielczosc
//                && e<(roundedOffset-poczatekWygenerowanejSekcji)/rozdzielczosc+(int)przesuniecie/rozdzielczosc+self.root.height+100
                && h*2<SECTIONS) {
//
                th[h*2] = CGPointMake(rozdzielczosc*h+roundedOffset-[Sparrow stage].width, self.game.level.poziomMorza-wysokosc);
                th[h*2+1] = CGPointMake(rozdzielczosc*h+roundedOffset-[Sparrow stage].width, Sparrow.stage.height+30);
                if (self.falaRender != nil) {
                    [self.falaRender.vertexData setPositionWithX:rozdzielczosc*h+roundedOffset-[Sparrow stage].width y:(self.game.level.poziomMorza-wysokosc)-x atIndex:h*2];
                    [self.falaRender.vertexData setPositionWithX:rozdzielczosc*h+roundedOffset-[Sparrow stage].width y:Sparrow.stage.height+3 atIndex:h*2+1];
                }
                
                h++;
            }
            e++;
        }
        lastx += B.dlugoscBloku/rozdzielczosc;
        lasty =  B.koniecBloku;
    }
    
    
}
- (void)onEnterFrame: (SPEnterFrameEvent *)event
{
    [self tableStep: event.passedTime];

}

- (void)tableStep: (float)passedTime
{
    [self generujTabliceDlaPoziomu:1 zRozdzielczoscia:10 zCzasem: passedTime];
    
}

- (void)step
{
    [self stepWithBodies:nil];
}
- (float)thForX: (int)x
{
    int sektor = x/rozdzielczoscGrafiki;
    sektor+=*_offset/10.0f;
    sektor+=1;
    int nieWyswietlana = Sparrow.stage.width/5;

    int index = sektor*2+nieWyswietlana;

    return (th[index].y)*TEXTURE_PERCENT+65;
    
}
#pragma mark - Fizyka
- (void)stepWithBodies: (NSMutableArray *)bodies
{
    if (th!=nil) {
        int sektorc = self.game.statek.bodyPhys->GetPosition().x*PTM_RATIO/rozdzielczoscGrafiki;
        sektorc += 2;
        sektorc+=*_offset/rozdzielczoscGrafiki;
        
        int nieWyswietlana = Sparrow.stage.width/5;
        
        float ia = (Sparrow.stage.height-th[sektorc*2+nieWyswietlana].y)*TEXTURE_PERCENT/PTM_RATIO;
        float ib = (Sparrow.stage.height-th[sektorc*2+2+nieWyswietlana].y)*TEXTURE_PERCENT/PTM_RATIO;
        
        self.game.statek.phRotation = atan(((ib*PTM_RATIO)-(ia*PTM_RATIO))/rozdzielczoscGrafiki);
        
        [self.game.statek calculateRoation];
        [self.game.statek moveToHigh:ia];
        

//        if (bodies!=NULL) {
//            for (int x=0; x<bodies.count; x++) {
//                FloatingBody *cargo = [bodies objectAtIndex:x];
//                
//                b2Body *body = cargo->body;
//                b2Vec2 srodek = body->GetWorldCenter();
//                
//                int sektor = body->GetPosition().x*PTM_RATIO/rozdzielczoscGrafiki;
//                sektor+=*_offset/10.0f;
//                sektor+=1;
//                if (sektor<0) {
//                    [cargo wyjsciePozaZakres];
//                    [bodies removeObject:cargo];
//                    continue;
//                }
//                double offset1 = (Sparrow.stage.height-th[sektor*2+nieWyswietlana].y);
//                double offset2 = (Sparrow.stage.height-th[sektor*2-2+nieWyswietlana].y);
//                double roznica = offset1-offset2;
//                double odleglosc2 = (double)rozdzielczoscGrafiki;
//                double odleglosc = body->GetPosition().x;
//                //TODO:asdasmnd
//                //TODO: Handle this error gracefully
//                //                fluidOffset = offset2/PTM_RATIO-h1;
//                //                double tangens = -atan(h1/odleglosc);
//                //                normal = b2Vec2(tangens, 1.0f);
//                fluidOffset = offset1/PTM_RATIO;
//                //                double a = (offset2-offset1)/odle
////                
//                if (srodek.y<=max(offset1, offset2)) {
//                    [self buoyancyStep:body];
//                }
//            }
//        }
    }
    
}
-(void)stepWithBody: (b2Body *)body
{
    int nieWyswietlana = Sparrow.stage.width/5;

    b2Vec2 srodek = body->GetWorldCenter();
    
    int sektor = body->GetPosition().x*PTM_RATIO/rozdzielczoscGrafiki;
    sektor+=*_offset/10.0f;
    sektor+=1;
    if (sektor<-Sparrow.stage.width/10 || body->GetPosition().y<-20) {
        SPSprite *myActor = (__bridge SPSprite*)body->GetUserData();
        if ([myActor isKindOfClass:[FloatingBody class]]) {
            [(FloatingBody *)myActor wyjsciePozaZakres];
        }else
        {
            body->GetWorld()->DestroyBody(body);

        }
//        [bodies removeObject:cargo];
    }
    int index = sektor*2+nieWyswietlana;
    if (index<SECTIONS) {
        body->SetActive(YES);

        double offset1 = (Sparrow.stage.height-th[index].y)*TEXTURE_PERCENT;
        double offset2 = (Sparrow.stage.height-th[index-2].y)*TEXTURE_PERCENT;
//        double roznica = offset1-offset2;
//        double odleglosc2 = (double)rozdzielczoscGrafiki;
//        double odleglosc = body->GetPosition().x;
        //                fluidOffset = offset2/PTM_RATIO-h1;
        //                double tangens = -atan(h1/odleglosc);
        //                normal = b2Vec2(tangens, 1.0f);
        fluidOffset = offset1/PTM_RATIO;
        //                double a = (offset2-offset1)/odle
        //
        if (srodek.y<=max(offset1, offset2)) {
            [self buoyancyStep:body];
        }
    }else
    {
        body->SetActive(NO);
    }

}
- (void)buoyancyStep:(b2Body *)body
{
    
    b2Vec2 gravity = body->GetWorld()->GetGravity();
    
    if(!body->IsAwake())
    {
        //Buoyancy force is just a function of position,
        //so unlike most forces, it is safe to ignore sleeping bodes
        return;
    }
    b2Vec2 areac(0,0);
    b2Vec2 massc(0,0);
    float32 area = 0;
    float32 mass = 0;
    for(b2Fixture* fixture=body->GetFixtureList(); fixture; fixture=fixture->GetNext())
    {
        b2Vec2 sc(0,0);
        float32 sarea = fixture->GetShape()->ComputeSubmergedArea(normal, fluidOffset, body->GetTransform(), &sc);
        area += sarea;
        areac.x += sarea * sc.x;
        areac.y += sarea * sc.y;
        float shapeDensity = 0;
        bool useDensity = YES ;
        
        if(useDensity)
        {
            shapeDensity=fixture->GetDensity();
        } else
        {
            shapeDensity = 0.50f;
        }
        mass += sarea*shapeDensity;
        massc.x += sarea * sc.x * shapeDensity;
        massc.y += sarea * sc.y * shapeDensity;
    }
    
    areac.x/=area;
    areac.y/=area;
    massc.x/=mass;
    massc.y/=mass;
    
    if(area < FLT_EPSILON)
    {
        ((__bridge FloatingBody *)body->GetUserData()).submerged = NO;
        return;
    }

    //Buoyancy
    b2Vec2 buoyancyForce = -density*area*gravity;
    //        NSLog(@"%f %f", density, area);
    
    body->ApplyForce(buoyancyForce,massc);
    //    body->ApplyForceToCenter(buoyancyForce);
    //Linear drag
    b2Vec2 dragForce = body->GetLinearVelocity() - velocity;
    
    dragForce *= -linearDrag * area;
    body->ApplyForce(dragForce,areac);
    //Angular drag
    body->ApplyTorque(-body->GetInertia()/body->GetMass()*area*body->GetAngularVelocity()*angularDrag);
    if (area < 0.01) {
        ((__bridge FloatingBody *)body->GetUserData()).submerged = NO;
    }else
        ((__bridge FloatingBody *)body->GetUserData()).submerged = YES;

}
@end
