//
//  MyContactListener.h
//  Save the ship!
//
//  Created by Piotrek on 18.01.2013.
//
//
#import "GunpowerBarrel.h"
#import "ColectableObject.h"
#import "CargoBox.h"
#import "Statek.h"
class myContactListiner : public b2ContactListener
{
public:
    
    void BeginContact(b2Contact* contact) {
        
        NSObject* bodyUserDataA =  (__bridge  NSObject *)contact->GetFixtureA()->GetBody()->GetUserData();
        NSObject* bodyUserDataB =  (__bridge  NSObject *)contact->GetFixtureB()->GetBody()->GetUserData();
        
        
        if ([bodyUserDataA isKindOfClass:GunpowerBarrel.class] && ([bodyUserDataB isKindOfClass:Statek.class] || [bodyUserDataB isKindOfClass:[CargoBox class]]))
        {
            [(GunpowerBarrel*)bodyUserDataA colisionWithShip];
        }
        if ([bodyUserDataB isKindOfClass:GunpowerBarrel.class] && ([bodyUserDataA isKindOfClass:Statek.class] || [bodyUserDataA isKindOfClass:[CargoBox class]]))
        {
            [(GunpowerBarrel*)bodyUserDataB colisionWithShip];
        }
        if ([bodyUserDataA isKindOfClass:CargoBox.class] && [bodyUserDataB isKindOfClass:Statek.class]) {
            ((CargoBox *)bodyUserDataA).towerLevel = 1;
//            [(CargoBox *)bodyUserDataA showDebugMarker];
        }
        if ([bodyUserDataB isKindOfClass:CargoBox.class] && [bodyUserDataA isKindOfClass:Statek.class]) {
            ((CargoBox *)bodyUserDataB).towerLevel = 1;
            
//            [(CargoBox *)bodyUserDataB showDebugMarker];
        }
  
        
    }
    
    void EndContact(b2Contact *contact) {
        NSObject* bodyUserDataA =  (__bridge NSObject *)contact->GetFixtureA()->GetBody()->GetUserData();
        NSObject* bodyUserDataB =  (__bridge  NSObject *)contact->GetFixtureB()->GetBody()->GetUserData();
        
        
        if ([bodyUserDataA isKindOfClass:CargoBox.class] && [bodyUserDataB isKindOfClass:Statek.class]) {
            ((CargoBox *)bodyUserDataA).towerLevel = 0;
            
//            [(CargoBox *)bodyUserDataA hideDenugMarker];
        }
        if ([bodyUserDataB isKindOfClass:CargoBox.class] && [bodyUserDataA isKindOfClass:Statek.class]) {
            ((CargoBox *)bodyUserDataB).towerLevel = 0;
            
//            [(CargoBox *)bodyUserDataB hideDenugMarker];
        }
        if ([bodyUserDataB isKindOfClass:CargoBox.class] && [bodyUserDataA isKindOfClass:CargoBox.class]) {
            if (((CargoBox*)bodyUserDataB).towerLevel != 1) {
                ((CargoBox*)bodyUserDataB).towerLevel = 0;
            }
            if (((CargoBox*)bodyUserDataA).towerLevel != 1) {
                ((CargoBox*)bodyUserDataA).towerLevel = 0;
            }


        }

        
    }
    void PreSolve(b2Contact* contact,
                  const b2Manifold* oldManifold) {
//        NSLog(@"Pre solve");
    }
    
    void PostSolve(b2Contact* contact,
                   const b2ContactImpulse* impulse) {
        b2Body *bodyA = contact->GetFixtureA()->GetBody();
        b2Body *bodyB = contact->GetFixtureB()->GetBody();
        
        NSObject* bodyUserDataA =  (__bridge  NSObject *)bodyA->GetUserData();
        NSObject* bodyUserDataB =  (__bridge  NSObject *)bodyB->GetUserData();
        
        float k = 2;
        if (impulse->normalImpulses[0]>0.5) {
            if ([bodyUserDataA isKindOfClass:CargoBox.class] && ([bodyUserDataB isKindOfClass:Statek.class]))
            {
                [(CargoBox*)bodyUserDataA stuk];
            }
            if ([bodyUserDataB isKindOfClass:CargoBox.class] && ([bodyUserDataA isKindOfClass:Statek.class]))
            {
                [(CargoBox*)bodyUserDataB stuk];
            }

        }
        
        if ([bodyUserDataA isKindOfClass:[CargoBox class]] && [bodyUserDataB isKindOfClass:[CargoBox class]]) {
            if (((CargoBox*)bodyUserDataB).towerLevel == 1 &&
                ((CargoBox*)bodyUserDataA).towerLevel != 1) {
                if (bodyA->GetWorldCenter().y>contact->GetFixtureB()->GetAABB(0).upperBound.y) {
                    ((CargoBox*)bodyUserDataA).towerLevel = 2;
                }
            }else if (((CargoBox*)bodyUserDataB).towerLevel > 1 &&
                      ((CargoBox*)bodyUserDataA).towerLevel == 0)
            {
                if (bodyA->GetWorldCenter().y>contact->GetFixtureB()->GetAABB(0).upperBound.y) {
                    ((CargoBox*)bodyUserDataA).towerLevel = ((CargoBox*)bodyUserDataB).towerLevel+1;
                }

            }
            if (((CargoBox*)bodyUserDataB).towerLevel != 1 &&
                ((CargoBox*)bodyUserDataA).towerLevel == 1) {
                if (bodyB->GetWorldCenter().y>contact->GetFixtureA()->GetAABB(0).upperBound.y) {

                    ((CargoBox*)bodyUserDataB).towerLevel = 2;
                }
            }else if (((CargoBox*)bodyUserDataA).towerLevel > 1 &&
                      ((CargoBox*)bodyUserDataB).towerLevel == 0)
            {
                if (bodyB->GetWorldCenter().y>contact->GetFixtureA()->GetAABB(0).upperBound.y) {
                    ((CargoBox*)bodyUserDataB).towerLevel = ((CargoBox*)bodyUserDataA).towerLevel+1;
                }
                
            }

        }
    }
};

