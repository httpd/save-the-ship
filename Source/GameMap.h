//
//  GameMap.h
//  Save the ship!
//
//  Created by Piotrek on 18.04.2013.
//
//

#import "SPDisplayObjectContainer.h"
@class MainGameStage;
@interface GameMap : SPDisplayObjectContainer
{
    SPSprite *map;
    int screenTouched;
}
@property (nonatomic, unsafe_unretained) MainGameStage *rootGameStage;

@end
