//
//  BoxDebug.m
//  Save the ship!
//
//  Created by Piotrek Knapczyk on 14.06.2012.
//  Copyright (c) 2012 IdeaStudio. All rights reserved.
//

#import "BoxDebug.h"

@implementation BoxDebug
@synthesize debugDraw= _debugDraw;

/** Create a debug layer with the given world and ptm ratio */
+(BoxDebug *)debugLayerWithWorld:(b2World *)world ptmRatio:(int)ptmRatio
{
    return [[BoxDebug alloc] initWithWorld:world ptmRatio:ptmRatio];
}

/** Create a debug layer with the given world, ptm ratio and debug display flags */
+(BoxDebug *)debugLayerWithWorld:(b2World *)world ptmRatio:(int)ptmRatio flags:(uint32)flags
{
    return [[BoxDebug alloc] initWithWorld:world ptmRatio:ptmRatio flags:flags];
}

/** Create a debug layer with the given world and ptm ratio */
-(id)initWithWorld:(b2World*)world ptmRatio:(int)ptmRatio
{
    return [self initWithWorld:world ptmRatio:ptmRatio flags:b2Draw::e_shapeBit];
}

/** Create a debug layer with the given world, ptm ratio and debug display flags */
-(id)initWithWorld:(b2World*)world ptmRatio:(int)ptmRatio flags:(uint32)flags
{
    if ((self = [self init])) {
        _boxWorld = world;
        _ptmRatio = ptmRatio;
        _debugDraw = new GLESDebugDraw( ptmRatio );
        _debugDraw->_screenHeight = [Sparrow stage].height;       _boxWorld->SetDebugDraw(_debugDraw);
        _debugDraw->SetFlags(flags);
    }
    
    return self;
}

/** Clean up by deleting the debug draw layer. */
-(void)dealloc
{
    _boxWorld = NULL;
    
    if ( _debugDraw != NULL ) {
        delete _debugDraw;
    }
    
}

/** Tweak a few OpenGL options and then draw the Debug Layer */
-(void)render:(SPRenderSupport *)support
{
    [super render:support];
    SPBaseEffect *baseEffect = [[SPBaseEffect alloc]init];
    
    [support finishQuadBatch]; // finish previously batched quads
    [support addDrawCalls:1]; // update stats display
    
    baseEffect.mvpMatrix = support.mvpMatrix;
    baseEffect.alpha = support.alpha;
    
    [baseEffect prepareToDraw];

//    glEnableClientState(GL_VERTEX_ARRAY);
//	glDisable(GL_TEXTURE_2D);
//	glDisableClientState(GL_COLOR_ARRAY);
//	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
//
    _debugDraw->effect = baseEffect;
    _debugDraw->renderSupport = support;
    _boxWorld->DrawDebugData();
    
//	glEnable(GL_TEXTURE_2D);
//	glEnableClientState(GL_COLOR_ARRAY);
//	glEnableClientState(GL_TEXTURE_COORD_ARRAY);    
//    glDisableClientState(GL_VERTEX_ARRAY);
}
@end
