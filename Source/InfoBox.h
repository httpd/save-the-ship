//
//  InfoBox.h
//  Save the ship!
//
//  Created by Piotrek on 04.07.2013.
//
//

#import "SPDisplayObjectContainer.h"

@interface InfoBox : SPSprite

@property (nonatomic, strong) SPTextField *title;
@property (nonatomic, strong) SPTextField *text;
@end
