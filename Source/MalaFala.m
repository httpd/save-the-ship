//
//  MalaFala.m
//  Save the ship!
//
//  Created by Piotrek on 21.05.2013.
//
//

#import "MalaFala.h"

@implementation MalaFala
- (id)init
{
    self = [super init];
    if (self) {
        przesuniecie=0;
        max =0;
        prog = 30;
        
        self.pozycja = 750;
        self.wysokoscFalki = 40;
        self.dlugosc = 240;
        
    }
    return self;
}
- (float)wysokoscDlaX: (int)x iReszty: (float)reszta
{
    if (self.typ==FalaWybuch) {
        if (x >= self.pozycja-przesuniecie && x<=self.pozycja) {
            float numer = x-self.pozycja+przesuniecie;
            if (numer<prog) {
                return self.wysokoscFalki*(-1+cos(numer/prog*M_PI))/2;
            }
            else
            {
                return self.wysokoscFalki*(-cos((numer-prog)/self.dlugosc*M_PI));
            }
        }
        
        if (x > self.pozycja && x<=self.pozycja+przesuniecie) {
            float numer = x-self.pozycja;
            
            float koniec = -prog+przesuniecie;
            if (numer<koniec) {
                return self.wysokoscFalki*(cos(( (numer+(self.dlugosc-koniec))/self.dlugosc*M_PI)));
            }
            else
            {
                return self.wysokoscFalki*(-1-cos((numer-koniec)/prog*M_PI))/2;
            }
        }
        
        
    }else if (self.typ==FalaSkrzynka)
    {
//        float reszta = fmodf(przesuniecie, 10.0f);
//        NSLog(@"%f",reszta);
            if (x >= self.pozycja-self.dlugosc/2-przesuniecie && x<=self.pozycja-prog-przesuniecie) {
                float numer = x-self.pozycja+self.dlugosc/2;
                
                return self.wysokoscFalki*sin((numer+przesuniecie)/(self.dlugosc/2)*M_PI*2)*numer/(self.dlugosc/2);
                
                
            }else if (x>self.pozycja-prog-przesuniecie && x<=self.pozycja-przesuniecie)
            {
                float numer = x-self.pozycja+self.dlugosc/2;
                float numerdwa = self.pozycja-x;
                return self.wysokoscFalki*sin((numer+przesuniecie)/(self.dlugosc/2)*M_PI*2)*numer/(self.dlugosc/2)*(numerdwa)/prog;
            }
            if (x > self.pozycja && x<=self.pozycja+przesuniecie) {
                
            }
            
        
    }
    return 0;
}
- (void)advanceTime:(double)seconds
{
    if (self.typ==FalaWybuch) {
        przesuniecie+=seconds*self.szybkosc;
        if (self.wysokoscFalki>0.1) {
            self.wysokoscFalki-=40*seconds;
        }else
        {
            [self dispatchEventWithType:SP_EVENT_TYPE_REMOVE_FROM_JUGGLER];
        }
        
    }else if (self.typ == FalaSkrzynka)
    {
        przesuniecie+=seconds*100.0f;
        if (self.wysokoscFalki>0) {
            //            self.wysokoscFalki-=40*seconds;
        }else
        {
            [self dispatchEventWithType:@"SP_EVENT_TYPE_REMOVE_FROM_JUGGLER"];
        }
    }
}
- (NSString *)debugDescription
{
    return [NSString stringWithFormat:@"MalaFala, typ: %d, pozycja %f, wyokosc %f, dlgosc: %f",self.typ,self.pozycja, self.wysokoscFalki, self.dlugosc ];
}
@end
