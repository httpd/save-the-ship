//
//  UIColor+Hex.m
//  Save the ship!
//
//  Created by Piotrek on 01.05.2013.
//
//

#import "UIColor+Hex.h"

@implementation UIColor (Hex)

- (NSUInteger)colorCode
{
    float red, green, blue;
    if ([self getRed:&red green:&green blue:&blue alpha:NULL])
    {
        NSUInteger redInt = (NSUInteger)(red * 255 + 0.5);
        NSUInteger greenInt = (NSUInteger)(green * 255 + 0.5);
        NSUInteger blueInt = (NSUInteger)(blue * 255 + 0.5);
        
        return (redInt << 16) | (greenInt << 8) | blueInt;
    }
    
    return 0;
}

@end