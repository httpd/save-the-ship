//
//  UIImage+ResizeImage.h
//  Save the ship!
//
//  Created by Piotrek on 14.12.2012.
//
//

#import <UIKit/UIKit.h>

@interface UIImage (ResizeImage)
+(UIImage *)resizeImage:(UIImage*)image newSize:(CGSize)newSize;
@end
