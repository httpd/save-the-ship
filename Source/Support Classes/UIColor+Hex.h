//
//  UIColor+Hex.h
//  Save the ship!
//
//  Created by Piotrek on 01.05.2013.
//
//

#import <UIKit/UIKit.h>

@interface UIColor (Hex)
- (NSUInteger)colorCode;
@end
