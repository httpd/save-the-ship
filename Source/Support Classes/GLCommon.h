//
//  GLCommon.h
//  AppScaffold
//
//  Created by Piotrek Knapczyk on 31.12.2011.
//  Copyright (c) 2011 IdeaStudio. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef struct {
    GLfloat red;
    GLfloat green;
    GLfloat blue;
    GLfloat alpha;
} ColorRGBA;

static inline ColorRGBA ColorRGBAMake(CGFloat inRed, CGFloat inGreen, CGFloat inBlue, CGFloat inAlpha) {
    ColorRGBA ret;
    ret.red = inRed;
    ret.green = inGreen;
    ret.blue = inBlue;
    ret.alpha = inAlpha;
    return ret;
}

typedef struct {
    GLfloat x;
    GLfloat y;
} Vertex2D;

static inline Vertex2D Vertex2DMake(CGFloat inX, CGFloat inY) {
    Vertex2D ret;
    ret.x = inX;
    ret.y = inY;
    return ret;
}
static inline void Vertex2DSet(Vertex2D *vertex, CGFloat inX, CGFloat inY) {
    vertex->x = inX;
    vertex->y = inY;
}

typedef Vertex2D Vector2D;

#define Vector2DMake(x,y) (Vector2D)Vertex2DMake(x,y)

#define Vector2DSet(vector,x,y) Vertex2DSet(vector,x,y)

static inline GLfloat Vector2DMagnitude(Vector2D vector) {
    return sqrtf((vector.x * vector.x) + (vector.y * vector.y));
}

static inline void Vector2DScaleToMagnitude(Vector2D *vector, GLfloat magnitude) {
    GLfloat vecMag = Vector2DMagnitude(*vector);
    if ( vecMag == 0.0 ) {
        vector->x = magnitude;
        vector->y = 0.0;
        return;
    }
    GLfloat multiplier = magnitude / vecMag;
    vector->x *= multiplier;
    vector->y *= multiplier;
}

typedef struct {
    Vertex2D vertex;
    ColorRGBA color;
} ColoredVertexData;