//
//  FloatingBody.m
//  Save the ship!
//
//  Created by Piotrek on 15.12.2012.
//
//

#import "FloatingBody.h"
#import "Game.h"
@implementation FloatingBody

@synthesize dragable, offset, floatingBodies, destroyed, submerged, draging = _draging;
- (void)setSubmerged:(bool)sub
{
    if (sub && !submerged && self.czas==1) {
        SPTween *t= [SPTween tweenWithTarget:self time:1];
        self.czas = 0;
        [t animateProperty:@"czas" targetValue:1];
        [self.game.juggler addObject:t];
//        self.alpha = 0.5;
        [self.game.fala addWaveAtPoint:self.x wysokosc:25 dlugosc:100 andSzybkosc:200];
        self.draging = false;
    }
    submerged = sub;
}
- (bool)submerged
{
    return submerged;
}
- (id)initWithWorld: (b2World *)_world
{
    self = [super init];
    if (self) {
        world = _world;
        self.destroyed = NO;
        self.submerged = NO;
        self.czas = 1;
    }
    return self;
}
- (void)wyjsciePozaZakres
{
    [floatingBodies removeObject:self];
    self.destroyed = YES;
}
- (void)testCatchAnimation
{

}


@end
