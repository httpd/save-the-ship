//
//  Piana.h
//  Save the ship!
//
//  Created by Piotrek on 01.09.2012.
//
//

#import <Foundation/Foundation.h>
#import "GLCommon.h"
#include "vector"
using namespace std;

@interface Piana : SPDisplayObject
{
    GLuint   texture[1];
    CGPoint th[110];
    CGPoint renderTh[380];
    GLfloat color[440];
    CGPoint texCoords[320];

    vector<CGPoint> maksima;
}
@property (nonatomic, assign) long long *offset;

@property CGPoint *tableOfHights;
@property int gamelenght;
@property int tableLenght;
@end
