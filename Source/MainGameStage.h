//
//  MainGameStage.h
//  Save the ship!
//
//  Created by Piotrek on 10.12.2012.
//
//

#import <Foundation/Foundation.h>

@class AppDelegate, PauseMenu, Game;
@interface MainGameStage : SPSprite
{
    SPSprite *mainGameContainer;
    Game *game;
    PauseMenu *pauza;
}
@property (unsafe_unretained) AppDelegate *delegate;
- (void)pauseGame;
- (void)startTestGame;
- (void)startLevel: (int)lev;
- (void)unpauseGame;
- (void)backToMenu;
- (void)showMap;

@property (nonatomic) float gameWidth;
@property (nonatomic) float gameHeight;
@end
