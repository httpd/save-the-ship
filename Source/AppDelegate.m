//
//  AppScaffoldAppDelegate.m
//  AppScaffold
//

#import "AppDelegate.h"
#import "MainGameStage.h"
@implementation AppDelegate
#pragma mark Application Delegate Methods
void onUncaughtException(NSException *exception)
{
    NSLog(@"uncaught exception: %@", exception);
}
- (void)applicationDidFinishLaunching:(UIApplication *)application 
{
//    NSSetUncaughtExceptionHandler(&onUncaughtException);
    CGRect screenBounds = [UIScreen mainScreen].bounds;

    mWindow = [[UIWindow alloc] initWithFrame:screenBounds];

//#ifdef DEBUG
//    [TestFlight setDeviceIdentifier:[[UIDevice currentDevice] uniqueIdentifier]];
//#endif
//    [TestFlight takeOff:@"8e38d47adf19769acf7408bc1c51fd28_MTQ2ODcyMjAxMi0xMC0yMyAxNjoyMjoxNy42NzY0NDI"];

    [SPAudioEngine start];

    mViewController = [[SPViewController alloc] init];
    [mViewController startWithRoot:[MainGameStage class] supportHighResolutions:YES doubleOnPad:YES];
    mViewController.showStats = YES;
    mViewController.preferredFramesPerSecond = 30.0f;
    mViewController.pauseOnWillResignActive = YES;
    [mWindow setRootViewController:mViewController];
    [mWindow makeKeyAndVisible];

}

- (void)applicationWillResignActive:(UIApplication *)application 
{
    
    [((MainGameStage *)mViewController.root) pauseGame];

        //    mViewController
//    if (theGameIsOn) {
//        [mSparrowView stop];
//    }
}

//- (void)applicationDidBecomeActive:(UIApplication *)application
//{

//    if (theGameIsOn) {
//        [mSparrowView start];
//    }
//}
//
//- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application
//{
//    if (theGameIsOn) {
//        [SPPoint purgePool];
//        [SPRectangle purgePool];
//        [SPMatrix purgePool];
//    }
//}
//
//- (void)dealloc 
//{
//    if (theGameIsOn) {
//        [SPAudioEngine stop];
//    }
//}
@end
