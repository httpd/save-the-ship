//
//  PauseMenu.m
//  Save the ship!
//
//  Created by Piotrek on 21.01.2013.
//
//

#import "PauseMenu.h"
#import "MainGameStage.h"
@implementation PauseMenu
- (id)init
{
    self = [super init];
    if (self) {

    }
    return self;
}
- (void)setup
{
    SPQuad *background = [SPQuad quadWithWidth:self.rootGameStage.gameWidth height:self.rootGameStage.gameHeight color:0x000000];
    background.alpha = .6;
    [self addChild:background];
    
    SPQuad *test = [SPQuad quadWithWidth:100 height:100 color:0x00FF00];
    test.y = self.rootGameStage.gameHeight-test.width;
    [self addChild:test];
    
    SPQuad *test2 = [SPQuad quadWithWidth:100 height:100 color:0xFF0000];
    test2.x = test2.width+20;
    test2.y = self.rootGameStage.gameHeight-test2.width;

    [self addChild:test2];
    
    
    [test addEventListener:@selector(unpauseGame:) atObject:self forType:SP_EVENT_TYPE_TOUCH];
    
    [test2 addEventListener:@selector(stopGame:) atObject:self forType:SP_EVENT_TYPE_TOUCH];

}
- (void)unpauseGame: (SPTouchEvent *)event
{
    NSSet *set = [event touchesWithTarget:self andPhase:SPTouchPhaseEnded];
    if ([set anyObject]) {
        [self.rootGameStage unpauseGame];
    }
}
- (void)stopGame: (SPTouchEvent *)event
{
    NSSet *set = [event touchesWithTarget:self andPhase:SPTouchPhaseEnded];
    if ([set anyObject]) {
        [self.rootGameStage backToMenu];
    }
}
@end
