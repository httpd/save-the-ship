//
//  Statek.h
//  Save the ship!
//
//  Created by Piotrek Knapczyk on 04.06.2012.
//  Copyright (c) 2012 IdeaStudio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Box2D/Box2D.h>
#import "GB2ShapeCache.h"
@class Game;
@interface Statek : SPDisplayObjectContainer
{
    SPSprite *obrazekStatku;
    b2World *world;
    bool alowAccRotation;
}
@property float szybkoscStatku;
@property (nonatomic) b2Body *bodyPhys;
@property float phRotation;
@property float accRotation;
@property (nonatomic, weak) Game *game;
@property int wsR; //wspolczynnik szybkosci obrotu
@property float physicalRotation;
@property SPQuad *area;
- (void)moveToHigh: (float)desiredHigh;
- (void)calculateRoation;

- (id)initWithWorld:(b2World *)_world andGame: (Game *)g;
- (void)initPhisicsWithPosision: (b2Vec2)p;
- (void)speedControl;

@end
