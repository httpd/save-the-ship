//
//  FalaRender.h
//  Save the ship!
//
//  Created by Piotrek Knapczyk on 24.07.2012.
//  Copyright (c) 2012 IdeaStudio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GLCommon.h"

@interface FalaRender : SPDisplayObject
{
    
    float alpha;
    BOOL complete;
    SPTexture *texture;
    SPRenderTexture *testTex;
    
    uint vertexBufferName;


}
- (id)initWithLevel: (int)lev;

- (void)setup;
@property BOOL complete;
@property int lenght;
@property long long *offset;
@property (strong) SPVertexData *vertexData;
@property  uint vertexBufferName;
@property (strong)   SPBaseEffect *baseEffect;
@end
