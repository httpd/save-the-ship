//
//  BoxDebug.h
//  Save the ship!
//
//  Created by Piotrek Knapczyk on 14.06.2012.
//  Copyright (c) 2012 IdeaStudio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Sparrow.h"
#import "GLES-Render.h"

@interface BoxDebug : SPSprite
{
    
    GLESDebugDraw* _debugDraw;  // The DebugDraw instance from GLES-Render
    b2World *_boxWorld;     // The Box2D world this is attached to
    int _ptmRatio;          // The PTM Ratio used in this world
}
+(BoxDebug *)debugLayerWithWorld:(b2World *)world ptmRatio:(int)ptmRatio;
+(BoxDebug *)debugLayerWithWorld:(b2World *)world ptmRatio:(int)ptmRatio flags:(uint32)flags;

/** Initialise a debug layer with the given parameters. */
-(id)initWithWorld:(b2World *)world ptmRatio:(int)ptmRatio;
-(id)initWithWorld:(b2World *)world ptmRatio:(int)ptmRatio flags:(uint32)flags;
@property  GLESDebugDraw* debugDraw;
@end
