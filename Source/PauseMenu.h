//
//  PauseMenu.h
//  Save the ship!
//
//  Created by Piotrek on 21.01.2013.
//
//

#import <Foundation/Foundation.h>
@class MainGameStage;
@interface PauseMenu : SPDisplayObjectContainer
{
    
}
@property (nonatomic, assign) MainGameStage *rootGameStage;
- (void)setup;
@end
