//
//  Statek.m
//  Save the ship!
//
//  Created by Piotrek Knapczyk on 04.06.2012.
//  Copyright (c) 2012 IdeaStudio. All rights reserved.
//

#import "Statek.h"
#import "Game.h"
@implementation Statek
@synthesize bodyPhys, accRotation, phRotation,wsR, szybkoscStatku;
- (void)setPhysicalRotation:(float)physicalRotation
{
    float nextAngle = self.bodyPhys->GetAngle() + self.bodyPhys->GetAngularVelocity() / 60.0;
    float totalRotation = physicalRotation - nextAngle;
    while ( totalRotation < -M_PI*2 ) totalRotation +=M_PI*2;
    while ( totalRotation >  M_PI*2 ) totalRotation -= M_PI*2;
    float desiredAngularVelocity = totalRotation * 60;
    float change = SP_D2R(1); //allow 1 degree rotation per time step
    desiredAngularVelocity = min( change, max(-change, desiredAngularVelocity));
    float impulse = self.bodyPhys->GetInertia() * desiredAngularVelocity;
    self.bodyPhys->ApplyAngularImpulse( impulse );
}
- (float)physicalRotation
{
    return self.bodyPhys->GetAngle();
}
- (id)initWithWorld:(b2World *)_world andGame: (Game *)g;
{
    self = [super init];
    if (self) {
        alowAccRotation = YES;
        self.game = g;
        self.szybkoscStatku = 0;
        [[GB2ShapeCache sharedShapeCache] addShapesWithFile:@"statek_1.plist"];
        obrazekStatku = [[SPSprite alloc] init];
        //test szumow
        
        SPTexture *texStatku = [self.game.atlas textureByName:@"statek_1"];
        
        SPImage *block = [[SPImage alloc] initWithTexture:texStatku];
        block.pivotX = block.width / 2;
        block.pivotY = block.height / 2;
        block.y-=32;
        [obrazekStatku addChild:block];
        
        [self addChild:obrazekStatku];
        wsR = 4;
        world = _world;
        
        self.area = [SPQuad quadWithWidth:120 height:80 color:0xFF0000];
        self.area.y = -50;
        self.area.pivotX = self.area.width/2;
        self.area.pivotY = self.area.height/2;
        self.area.visible = NO;
        [self addChild:self.area];
    }
    return self;
}
#pragma mark Phisics
- (void)initPhisicsWithPosision: (b2Vec2)p
{
    
	b2BodyDef bodyDef;
	bodyDef.type = b2_kinematicBody;
    bodyDef.userData = (__bridge void *)self;
    
    bodyDef.linearDamping = 0.0f;
    bodyDef.angularDamping = 0.0f;
    bodyDef.fixedRotation = NO;
    bodyDef.position = p;
	b2Body *statek = world->CreateBody(&bodyDef);
    self.bodyPhys= statek;

    self.szybkoscStatku = 70.0f/PTM_RATIO;

    [[GB2ShapeCache sharedShapeCache] addFixturesToBody:self.bodyPhys forShapeName:@"statek_1"];
}
- (void)moveToHigh: (float)desiredHigh
{
    float move = desiredHigh-self.bodyPhys->GetPosition().y;
    self.bodyPhys->SetLinearVelocity(b2Vec2(self.bodyPhys->GetLinearVelocity().x, move*12));
}
- (void)speedControl
{
    bodyPhys->SetLinearVelocity(b2Vec2(self.szybkoscStatku/PTM_RATIO, bodyPhys->GetLinearVelocity().y));

}
- (void)calculateRoation
{
    if (alowAccRotation) {
        float nextAngle = (self.phRotation+self.accRotation-self.bodyPhys->GetAngle());
        self.bodyPhys->SetAngularVelocity(nextAngle*wsR);
    }
}
@end
