//
//  CargoBox.h
//  Save the ship!
//
//  Created by Piotrek Knapczyk on 18.07.2012.
//  Copyright (c) 2012 IdeaStudio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FloatingBody.h"
@class Game;
@interface CargoBox : FloatingBody
{
    SPTween *tween;
    SPImage *znak;
    
    SPImage *oczy;
    
    SPBaseEffect *baseEffect;
    SPVertexData *vertexData;
    
    ushort *indexData;
    ushort numIndices;
    
    uint vertexBufferName;
    uint indexBufferName;
    NSArray *bodies;
    SPTween *sinkTween;
//    float gWidth;
//    float gHeight;
    SPQuad *bublesTest;
    
    SPQuad *debugMarker;
    
    SPQuad *kolo;
    SPSoundChannel *stuk;
}
@property bool niezatapialny;
@property int towerLevel;
@property (strong) SXParticleSystem *emitter;
@property int randomInt;
@property float32 density;
@property SPSoundChannel *channel;
- (id)initWithWorld: (b2World *)_world andGame:(Game *)g;
- (void)showDebugMarker;
- (void)hideDenugMarker;
- (void)removeKolo;
-(void)stuk;
@end
