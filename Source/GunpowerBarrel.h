//
//  GunpowerBarrel.h
//  Save the ship!
//
//  Created by Piotrek on 17.01.2013.
//
//

#import <Foundation/Foundation.h>
#import "FloatingBody.h"
#import "Statek.h"
@class Game;
@interface GunpowerBarrel : FloatingBody
{
    NSMutableArray *blocks;
    float sila;
    float zasieg;
}
@property (nonatomic, strong) SXParticleSystem *system;
@property SPSoundChannel *channel;
- (id)initWithWorld: (b2World *)_world andGame:(Game *)g;
- (void)colisionWithShip;
- (void)executePhysics;
@end

