//
//  ColectableObject.m
//  Save the ship!
//
//  Created by Piotrek on 17.04.2013.
//
//

#import "ColectableObject.h"
#import "CargoBox.h"
#import "Game.h"
@implementation ColectableObject
- (id)init
{
    self = [super init];
    if (self) {
        SPQuad *quad = [SPQuad quadWithWidth:10 height:10 color:0x00FF00];
        [self addChild:quad];
        self.colected = NO;
        self.tween = nil;
        self.destination = nil;
        self.animationCompletion = 0;
        [self addEventListener:@selector(onEnterFrame:) atObject:self forType:SP_EVENT_TYPE_ENTER_FRAME];
    }
    return self;
}
- (void)startFolowing: (CargoBox *)obiekt
{
    self.destination = obiekt;
//    SPTween *tween = [SPTween tweenWithTarget:self time:1];
//    [tween animateProperty:@"animationCompletion" targetValue:1];
//    [obiekt.game.juggler addObject:tween];
}
- (void)onEnterFrame: (SPEnterFrameEvent *)event
{
    //MARK: Możliwe problemy z efektownością
    if (self.destination != Nil ) {
        SPPoint *vectorPrzemieszczenia = [SPPoint pointWithX:self.destination.x-self.x y:self.destination.y-self.y];
        vectorPrzemieszczenia = [vectorPrzemieszczenia normalize];
        vectorPrzemieszczenia = [vectorPrzemieszczenia scaleBy:15];
        self.x += vectorPrzemieszczenia.x;
        self.y += vectorPrzemieszczenia.y;
    }
}
@end
