//
//  Game.h
//  AppScaffold
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import <CoreMotion/CoreMotion.h>

#import "Fala.h"
#import "Statek.h"
#import "GB2ShapeCache.h"
#import "Sparrow.h"
#import "MyContactListener.h"
#import "GLES-Render.h"
#import "BoxDebug.h"
#import "CargoBox.h"
#import "NieboTlo.h"
#import "GunpowerBarrel.h"
#import "InfoBox.h"
#import "EndLevelInfo.h"
#import "Level.h"
@class MainGameStage;
@interface Game : SPSprite
{
    
    SPButton *pauzaButton;
    SPImage *touchZone;
    
    float gWidth;
    float gHeight;    
    b2World* _world;
    NSTimer *updateTimer;
    CFTimeInterval lastTime;
    int _positionIterations, _velocityIterations;
    myContactListiner *myContactListenerInstance;
    NSTimer *phTimer;
    SPImage *block;
    CMMotionManager *motionManager;
    GLESDebugDraw *m_debugDraw;
    double filteredAcceleration[3];
    float timeAccumulated;
    float xf, yf;
    double alfa;
        NSMutableArray *floatigBodies;

    int count;
    
    long long *offset;
    
    int camx;
    int camy;
    
    NSMutableArray *monety;
    
    SPTween *scaleTween;
    //OGL
    GLuint fboId;
    GLuint textureId;

    bool fboUsed;
    
    bool firstStop;

    UIInterfaceOrientation orientation;

    SPSprite *explosionLayer;
    SPSprite *bombLayer;
    
    
    BoxDebug *debuf;
    
    EndLevelInfo *info;
    
    bool endgame;
    b2MouseJoint *mouseJoint;
    b2Body *groundBody;
    
    SPTextField *punktyTextField;
    SPSoundChannel *backgoundMusic;
    SPSoundChannel *backgoundNoise;

    SPTextField *comboTextField;
    }
- (void)unPause;
- (void)pause;
- (void)gameOver;
- (void)explosionOnPoint:(SPPoint *)point;

@property (nonatomic, strong) NSTimer *physTimer;
@property (nonatomic, strong) NSTimer *motionTimer;
@property (nonatomic, unsafe_unretained) MainGameStage *rootGameStage;
- (id)initWithWidth:(float)width height:(float)height andGameMode: (gameMode)mode andLevel: (int)lev;
- (void)strataSkrzyni;
- (void)end;
@property (nonatomic, strong) SPSprite *gameContainer;
@property (nonatomic, strong) SPSprite *backgroundContainer;
@property (nonatomic, strong) SPSprite *GUIContainer;
@property (nonatomic, strong) SPSprite *GameGUI;
@property (nonatomic, strong) LevelClass* level;
@property (nonatomic, strong) Fala *fala;
@property (nonatomic, strong) Statek *statek;
@property (nonatomic, strong) SPJuggler *juggler;
//@property (nonatomic, strong) SXParticleSystem *explosionEmiter;
@property (nonatomic, strong) SPTextureAtlas *atlas;
@property (nonatomic, strong) InfoBox *helpBox;
@property (nonatomic) bool gameOverBool;
@property (nonatomic) float punkty;
@property (nonatomic) int combo;
@end
