//
//  CargoBox.m
//  Save the ship!
//
//  Created by Piotrek Knapczyk on 18.07.2012.
//  Copyright (c) 2012 IdeaStudio. All rights reserved.
//

#import "CargoBox.h"
#import "UIImage+ResizeImage.h"
#import "Game.h"
@implementation CargoBox
@synthesize towerLevel = _towerLevel;
- (void)setTowerLevel:(int)tl
{
    _towerLevel = tl;
//    if (tl==3) {
//        [self showDebugMarker];
//    }else
//    {
//        [self hideDenugMarker];
//    }
}
- (int)towerLevel
{
    return _towerLevel;
}
- (void)setDensity:(float32)density
{
    
    if (body->IsActive()  && !self.destroyed) {
        for (b2Fixture* f = body->GetFixtureList(); f; f = f->GetNext())
        {
            f->SetDensity(density);
        }
        body->ResetMassData();
    }
}
- (float32)density
{
    return body->GetFixtureList()->GetDensity();
}
-(void)stuk
{
    SPSound *s = [SPSound soundWithContentsOfFile:@"Monety.mp3"];
    stuk = [s createChannel];
    [s play];
}
- (void)setSubmerged:(bool)submerged
{
//    NSLog(@"%@",submerged?@"sumberged":@"alive");
    if (submerged && !self.submerged) {

        sinkTween = [SPTween tweenWithTarget:self time:1];
        [sinkTween addEventListener:@selector(sinkThisShit:) atObject:self forType:SP_EVENT_TYPE_REMOVE_FROM_JUGGLER];
        [self.game.juggler addObject:sinkTween];
             [self.channel play];
    }else if(!submerged)
    {
        if (sinkTween) {
//            [self.emitter stop];
            [self.game.juggler removeObject:sinkTween];
            sinkTween= nil;
            self.density = 1.f;
            NSLog(@"alive");
        }
             [self.channel pause];
    }
    [super setSubmerged:submerged];


}
- (void)sinkThisShit: (SPEvent *)event
{
    if (!self.niezatapialny) {
        NSLog(@"dead");
        //    [self.emitter start];
        [self.channel pause];

        self.density = 2.5f;
    }
}
- (id)initWithWorld: (b2World *)_world andGame:(Game *)g
{
    self = [super initWithWorld:_world];
    if (self) {
        
        self.game = g;
//        self.blendMode = SP_BLEND_MODE_NORMAL;
        oczy = [SPImage imageWithTexture:[self.game.atlas textureByName:@"oczy"]];

        SPQuad *image = [[SPQuad alloc] initWithWidth:arc4random()%15+oczy.width+4 height:arc4random()%15+oczy.height+10];
//
//        image.scaleX = 0.3;
//        image.scaleY = 0.3;
        image.pivotX = image.width/2;
        image.pivotY = image.height/2;
        image.color = 0x000000;
        image.alpha = 0;
        [self addChild:image];
        
        uint kolorOczu = 0x173747;
        SPQuad *leweOko = [SPQuad quadWithWidth:5 height:5 color:kolorOczu];
        leweOko.x = -8;
        leweOko.y = -6;
        [self addChild:leweOko];
        
        SPQuad *praweOko = [SPQuad quadWithWidth:5 height:5 color:kolorOczu];
        praweOko.x = 3;
        praweOko.y = -6;    
        [self addChild:praweOko];

        
        
        oczy.pivotX = oczy.width/2;
        oczy.pivotY = oczy.height/2;
        [self addChild:oczy];
        
        float bumperSize = 5;
        float rectRadius = 5;
        int iloscSekcji = 3;
        uint kolorBumpera = 0x000000;


        b2BodyDef bodyDef;
        bodyDef.position.Set((180.0f+arc4random()%200)/PTM_RATIO, (350.0f+(arc4random()%200))/PTM_RATIO);
        bodyDef.type = b2_dynamicBody;
        bodyDef.angularDamping = 5;
        
        bodyDef.userData = (__bridge void *)self;
        body = _world->CreateBody(&bodyDef);
        
        b2PolygonShape polygon;
        polygon.SetAsBox((image.width+2*bumperSize-2)/2/PTM_RATIO, (image.height-2)/2/PTM_RATIO);
        
        b2FixtureDef fixtureDef;
        fixtureDef.shape = &polygon;
        fixtureDef.density = 1.0f;
        

        fixtureDef.friction = 0.4f;
        fixtureDef.restitution = 0.1f;
//        
//        
//        fixtureDef.friction = 0.5f;
//        fixtureDef.restitution = .20;
        
//        fixtureDef.filter.maskBits = 0x0001;
//        fixtureDef.filter.categoryBits = 0x0001;
        fixtureDef.filter.maskBits = 0x003;
        fixtureDef.filter.categoryBits = 0x003;        
        body->CreateFixture(&fixtureDef);
//        [self loadTexture];
        
        baseEffect = [[SPBaseEffect alloc] init];
        vertexData = [[SPVertexData alloc] init];
        SPTexture *texture = [SPTexture textureWithContentsOfFile:@"skrzynie.png"];
        texture.repeat = NO;
        
        int lastIndex=0;
        int lastvertex=0;
        vertexData.numVertices = 100;
        
//        baseEffect.texture = texture;
//        baseEffect.useTinting = NO;
        self.blendMode = SP_BLEND_MODE_NORMAL;
        baseEffect.premultipliedAlpha = NO;
        
        
        float gradient[6]= {0.0, 0.09, 0.32, 0.70, 0.9, 1};
        float hue = (double)arc4random()/0x100000000;
        

        int gradientKolor[6] = {
            [UIColor colorWithHue:hue saturation:0.5 brightness:1 alpha:1].colorCode,
            [UIColor colorWithHue:hue saturation:0.5 brightness:1 alpha:1].colorCode,
            [UIColor colorWithHue:hue saturation:0.8 brightness:0.8 alpha:1].colorCode,
            [UIColor colorWithHue:hue saturation:0.8 brightness:0.8 alpha:1].colorCode,
            [UIColor colorWithHue:hue saturation:0.8 brightness:0.4 alpha:1].colorCode,
            [UIColor colorWithHue:hue saturation:0.8 brightness:0.4 alpha:1].colorCode
        };

        for (int i=0; i<2; i++) {
            for (int e=0; e<8; e++) {
                [vertexData setPositionWithX:-image.width/2+i*image.width
                                           y:-image.height/2+gradient[e]*image.height
                                     atIndex:e*2+i];
                [vertexData setColor:gradientKolor[e] atIndex:e*2+i];
                
            }
        }
        indexData = new ushort[200];
        
        for (int i=0; i<10; i++) {
            for (int n=0; n<3; n++) {
                indexData[i*3+n] = i+n;
                lastIndex = i*3+n;
                lastvertex = i+n;
            }
        }
        lastIndex++;
        lastvertex++;
        
        int firstcenter = lastvertex++;
        [vertexData setPositionWithX:-image.width/2-(bumperSize-rectRadius) y:-image.height/2+rectRadius atIndex:firstcenter];
        [vertexData setColor:kolorBumpera atIndex:firstcenter];

        for (int i=0; i<=iloscSekcji; i++) {
            float radians = M_PI_2/(float)iloscSekcji*i;
            
            float x = sinf(radians) * rectRadius;
            float y = cosf(radians) * rectRadius;
            
            [vertexData setPositionWithX:-image.width/2-(bumperSize-rectRadius)-x
                                       y:-image.height/2+rectRadius-y
                                 atIndex:lastvertex+i];
            [vertexData setColor:0x000000 atIndex:lastvertex+i];

//            NSLog(@"Postion at Index: %d, X: %f, Y:%f", lastvertex+i,-self.width/2-(bumperSize-rectRadius)-x, -self.height/2+rectRadius-y);
            if (i!=0) {
                indexData[lastIndex++]=firstcenter;
                indexData[lastIndex++]=lastvertex+i-1;
                indexData[lastIndex++]=lastvertex+i;
            }

        }
//        NSLog(@"lastIndex: %d lastVertex %d", lastIndex, lastvertex);
        lastvertex +=iloscSekcji;
//        NSLog(@"lastIndex: %d lastVertex %d", lastIndex, lastvertex);
        lastvertex++;
        int seccenter = lastvertex++;
        [vertexData setPositionWithX:-image.width/2-(bumperSize-rectRadius) y:image.height/2-rectRadius atIndex:seccenter];
        [vertexData setColor:0x000000 atIndex:seccenter];
        
        for (int i=0; i<=iloscSekcji; i++) {
            float radians = M_PI_2/(float)iloscSekcji*i;
            
            float x = sinf(radians) * rectRadius;
            float y = cosf(radians) * rectRadius;
            
            [vertexData setPositionWithX:-image.width/2-(bumperSize-rectRadius)-x
                                       y:image.height/2-rectRadius+y
                                 atIndex:lastvertex+i];
            [vertexData setColor:0x000000 atIndex:lastvertex+i];
            
//            NSLog(@"Postion at Index: %d, X: %f, Y:%f", lastvertex+i,-self.width/2-(bumperSize-rectRadius)-x, -self.height/2+rectRadius-y);
            if (i!=0) {
                indexData[lastIndex++]=seccenter;
                indexData[lastIndex++]=lastvertex+i-1;
                indexData[lastIndex++]=lastvertex+i;
            }
            
        }
        lastvertex+=iloscSekcji+1;
        
        indexData[lastIndex++]=firstcenter;
        indexData[lastIndex++]=seccenter-1;
        indexData[lastIndex++]=seccenter;

        indexData[lastIndex++]=seccenter;
        indexData[lastIndex++]=seccenter-1;
        indexData[lastIndex++]=seccenter+iloscSekcji+1;

        indexData[lastIndex++]=firstcenter+1;
        indexData[lastIndex++]=lastvertex;
        indexData[lastIndex++]=lastvertex+1;

        indexData[lastIndex++]=seccenter+1;
        indexData[lastIndex++]=firstcenter+1;
        indexData[lastIndex++]=lastvertex+1;

        [vertexData setPositionWithX:-image.width/2 y:-image.height/2 atIndex:lastvertex++];
        [vertexData setPositionWithX:-image.width/2 y:image.height/2 atIndex:lastvertex++];

        //prawa strona
//        lastIndex++;
//        lastvertex++;
        firstcenter = lastvertex++;
        [vertexData setPositionWithX:image.width/2+(bumperSize-rectRadius) y:-image.height/2+rectRadius atIndex:firstcenter];
        [vertexData setColor:0x000000 atIndex:firstcenter];
        
        for (int i=0; i<=iloscSekcji; i++) {
            float radians = M_PI_2/(float)iloscSekcji*i;
            
            float x = sinf(radians) * rectRadius;
            float y = cosf(radians) * rectRadius;
            
            [vertexData setPositionWithX:image.width/2+(bumperSize-rectRadius)+x
                                       y:-image.height/2+rectRadius-y
                                 atIndex:lastvertex+i];
            [vertexData setColor:0x000000 atIndex:lastvertex+i];
            
//            NSLog(@"Postion at Index: %d, X: %f, Y:%f", lastvertex+i,-self.width/2-(bumperSize-rectRadius)-x, -self.height/2+rectRadius-y
//                  );
            if (i!=0) {
                indexData[lastIndex++]=firstcenter;
                indexData[lastIndex++]=lastvertex+i-1;
                indexData[lastIndex++]=lastvertex+i;
            }
            
        }
//        NSLog(@"lastIndex: %d lastVertex %d", lastIndex, lastvertex);
        lastvertex +=iloscSekcji;
//        NSLog(@"lastIndex: %d lastVertex %d", lastIndex, lastvertex);
        lastvertex++;
        seccenter = lastvertex++;
        [vertexData setPositionWithX:image.width/2+(bumperSize-rectRadius) y:image.height/2-rectRadius atIndex:seccenter];
        [vertexData setColor:0x000000 atIndex:seccenter];
        
        for (int i=0; i<=iloscSekcji; i++) {
            float radians = M_PI_2/(float)iloscSekcji*i;
            
            float x = sinf(radians) * rectRadius;
            float y = cosf(radians) * rectRadius;
            
            [vertexData setPositionWithX:image.width/2+(bumperSize-rectRadius)+x
                                       y:image.height/2-rectRadius+y
                                 atIndex:lastvertex+i];
            [vertexData setColor:0x000000 atIndex:lastvertex+i];
            
//            NSLog(@"Postion at Index: %d, X: %f, Y:%f", lastvertex+i,-self.width/2-(bumperSize-rectRadius)-x, -self.height/2+rectRadius-y);
            if (i!=0) {
                indexData[lastIndex++]=seccenter;
                indexData[lastIndex++]=lastvertex+i-1;
                indexData[lastIndex++]=lastvertex+i;
            }
            
        }
        lastvertex+=10;
        
        indexData[lastIndex++]=firstcenter;
        indexData[lastIndex++]=seccenter-1;
        indexData[lastIndex++]=seccenter;
        
        indexData[lastIndex++]=seccenter;
        indexData[lastIndex++]=seccenter-1;
        indexData[lastIndex++]=seccenter+iloscSekcji+1;

        indexData[lastIndex++]=seccenter;
        indexData[lastIndex++]=seccenter-1;
        indexData[lastIndex++]=seccenter+iloscSekcji;
        
        indexData[lastIndex++]=firstcenter+1;
        indexData[lastIndex++]=lastvertex;
        indexData[lastIndex++]=lastvertex+1;
        
        indexData[lastIndex++]=seccenter+1;
        indexData[lastIndex++]=firstcenter+1;
        indexData[lastIndex++]=lastvertex+1;
        
        [vertexData setPositionWithX:image.width/2 y:-image.height/2 atIndex:lastvertex++];
        [vertexData setPositionWithX:image.width/2 y:image.height/2 atIndex:lastvertex++];

        numIndices = lastIndex;
        [self createBuffers];
        
        
        self.emitter = [SXParticleSystem particleSystemWithContentsOfFile:@"babelki.pex"];
        self.emitter.scaleX = 1;
        self.emitter.scaleY = -1;
        self.emitter.x = 0;
        self.emitter.y = 0;
        self.emitter.bounds.width = 10;
        self.emitter.bounds.height = 10;
        [self addChild:self.emitter];
        self.niezatapialny = NO;
        if (g.level.levelNumber == 0) {
//            kolo = [SPQuad quadWithWidth:image.width+20 height:10 color:0xFF0000];
//            kolo.pivotX = kolo.width/2;
//            //kolo.pivotY = kolo.height/2;
//            self.niezatapialny = YES;
//            [self addChild:kolo];
        }
//        self.emitter.rot
//        [self.game.juggler addObject:self.emitter];
//        [self.emitter start];
        
//        int offset = 2;
//        float t = 0.5;
//
//        SPTween *etween = [SPTween tweenWithTarget:oczy time:t transition:SP_TRANSITION_EASE_IN_OUT];
//        [etween animateProperty:@"x" targetValue:oczy.x+offset];
//        etween.repeatCount = 0;
//        etween.reverse = YES;
//        [self.game.juggler addObject:etween];
//
//        SPTween *etween1 = [SPTween tweenWithTarget:praweOko time:t transition:SP_TRANSITION_EASE_IN_OUT];
//        [etween1 animateProperty:@"x" targetValue:praweOko.x+offset];
//        etween1.repeatCount = 0;
//        etween1.reverse = YES;
//        [self.game.juggler addObject:etween1];
//
//        SPTween *etween2 = [SPTween tweenWithTarget:leweOko time:t transition:SP_TRANSITION_EASE_IN_OUT];
//        [etween2 animateProperty:@"x" targetValue:leweOko.x+offset];
//        etween2.repeatCount = 0;
//        etween2.reverse = YES;
//        [self.game.juggler addObject:etween2];
//
//        oczy.x=-offset;
//        praweOko.x-=offset;
//        leweOko.x-=offset;
        [self flatten];
        
        self.towerLevel = 0;
        
        SPSound *sound = [SPSound soundWithContentsOfFile:@"Babelki.mp3"];
        self.channel = [sound createChannel];
   
        self.channel.loop = YES;
    }
    return self;
}
- (void)removeKolo
{
    self.niezatapialny = NO;
    SPMatrix *m = [self transformationMatrixToSpace:self.game.gameContainer];
    SPPoint *p = [SPPoint pointWithX:kolo.x y:kolo.y];
    p = [m transformPoint:p];
    [kolo removeFromParent];
    [self.game.gameContainer addChild:kolo];
    kolo.x = p.x; kolo.y = p.y;
//    kolo.pivotY = 0;
//    kolo.pivotX = 0;
    kolo.rotation = self.rotation;
    SPTween *tw = [SPTween tweenWithTarget:kolo time:1 transition:SP_TRANSITION_EASE_OUT];
    [tw fadeTo:0];
    [tw addEventListenerForType:SP_EVENT_TYPE_COMPLETED block:^(id event){
        [kolo removeFromParent];
        kolo = nil;
    }];
    [self.game.juggler addObject:tw];
}
- (void)printIndexData
{
    for (int i=0; i<numIndices; i++) {
        if (i%3==0) {
            NSLog(@"--");
        }

        NSLog(@"IndexData, vertex: %d at index: %d", indexData[i], i);
    }
}
- (void)preparePhisics
{
    
}
- (void)createBuffers
{
    if (vertexBufferName) glDeleteBuffers(1, &vertexBufferName);
    if (indexBufferName) glDeleteBuffers(1, &indexBufferName);
    
    glGenBuffers(1, &vertexBufferName);
    glGenBuffers(1, &indexBufferName);
    
    if (!vertexBufferName || !indexBufferName)
        [NSException raise:SP_EXC_OPERATION_FAILED format:@"could not create vertex buffers"];
    glBindBuffer(GL_ARRAY_BUFFER, vertexBufferName);
    glBufferData(GL_ARRAY_BUFFER, sizeof(SPVertex) * vertexData.numVertices, vertexData.vertices, GL_STATIC_DRAW);
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferName);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(ushort) * numIndices, indexData, GL_STATIC_DRAW);
    int attribPosition = baseEffect.attribPosition;
    int attribColor = baseEffect.attribColor;
    glEnableVertexAttribArray(attribPosition);
    glEnableVertexAttribArray(attribColor);

}

#pragma mark - OpenGL

- (void)wyjsciePozaZakres
{
    if (!self.destroyed) {
        self.destroyed = YES;
        if (self.game.level.levelNumber == 0 && [self containsChild:kolo]) {
            self.game.level.skrzynkiZKolem--;
        }
        //    self.game.le
        [self.emitter stop];
        //    [self.emitter removeFromParent];
        
        //    [super wyjsciePozaZakres];
        //    [self.floatingBodies removeObject:self];
        znak = [SPImage imageWithTexture:[self.game.atlas textureByName:@"strataSkrzynki"]];
        
        if (self.y<0) {
            znak.y = 0;
        }else if (self.y>self.stage.width-znak.height)
        {
            znak.y = self.stage.width-znak.height;
        }else
        {
            znak.y = self.y;
            
        }
        znak.x = 0;
        znak.alpha = 0;
        [((Game *)(self.parent.parent)).GUIContainer addChild:znak];
        tween = [SPTween tweenWithTarget:znak time:0.5f transition:SP_TRANSITION_EASE_OUT];
        [tween animateProperty:@"alpha" targetValue:1];
        [tween addEventListener:@selector(fadeOut:) atObject:self forType:SP_EVENT_TYPE_REMOVE_FROM_JUGGLER];
        [Sparrow.juggler addObject:tween];
    }
}
- (void)fadeOut: (SPEvent *)event
{
    tween = [SPTween tweenWithTarget:znak time:0.5f transition:SP_TRANSITION_EASE_IN];
    [tween animateProperty:@"alpha" targetValue:0];
    [tween addEventListener:@selector(done:) atObject:self forType:SP_EVENT_TYPE_REMOVE_FROM_JUGGLER];
    [Sparrow.juggler addObject:tween];

    
}
- (void)done: (SPEvent *)event
{
    [znak removeFromParent];
    [super wyjsciePozaZakres];

}
- (void)dealloc
{
    if (tween!=nil) {
        [tween removeEventListenersAtObject:self forType:SP_EVENT_TYPE_REMOVE_FROM_JUGGLER];
        [Sparrow.juggler removeObject:tween];
    }
}
- (void)render:(SPRenderSupport *)support
{

    [support finishQuadBatch]; // finish previously batched quads
    [support addDrawCalls:1]; // update stats display
    
    baseEffect.mvpMatrix = support.mvpMatrix;
    baseEffect.alpha = support.alpha;
    
    [baseEffect prepareToDraw];
    [SPBlendMode applyBlendFactorsForBlendMode:support.blendMode
                            premultipliedAlpha:vertexData.premultipliedAlpha];
    
    int attribPosition = baseEffect.attribPosition;
    int attribColor = baseEffect.attribColor;
//    glEnableVertexAttribArray(attribPosition);
//    glEnableVertexAttribArray(attribColor);

//    glEnable(GL_BLEND);
//    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
//    glHint(GL_POLYGON_SMOOTH, GL_NICEST);
//    glEnable(GL_POLYGON_SMOOTH);
    
//    glBindBuffer(GL_ARRAY_BUFFER, vertexBufferName);
//    if (vertexBufferName) glDeleteBuffers(1, &vertexBufferName);

//    glGenBuffers(1, &vertexBufferName);
//    if (!vertexBufferName)
//        [NSException raise:SP_EXC_OPERATION_FAILED format:@"could not create vertex buffers"];
//    glBindBuffer(GL_ARRAY_BUFFER, vertexBufferName);
//    glBufferData(GL_ARRAY_BUFFER, sizeof(SPVertex) * vertexData.numVertices, vertexData.vertices, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferName);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBufferName);


    glVertexAttribPointer(attribPosition, 2, GL_FLOAT, GL_FALSE, sizeof(SPVertex), (void *)(offsetof(SPVertex, position)));
    glVertexAttribPointer(attribColor, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(SPVertex),(void *)(offsetof(SPVertex, color)));
    
    glDrawElements(GL_TRIANGLES , numIndices, GL_UNSIGNED_SHORT, 0);

//    glBindBuffer(GL_ARRAY_BUFFER, vertexBufferName);
//    glBufferData(GL_ARRAY_BUFFER, sizeof(SPVertex) * leftBumper.numVertices, leftBumper.vertices, GL_STATIC_DRAW);
//
//
//    glDrawArrays(GL_TRIANGLE_STRIP, 0, (GLsizei)leftBumper.numVertices);

    [super render:support];
    
//    [self hideDenugMarker];
    
}
//- (SPRectangle*)boundsInSpace:(SPDisplayObject*)targetSpace
//{
//    SPMatrix *matrix = [self transformationMatrixToSpace:targetSpace];
//    return [vertexData boundsAfterTransformation:matrix];
//}
- (void)showDebugMarker
{
    NSLog(@"colision!");
    [self unflatten];
    if (debugMarker==nil) {
        debugMarker  = [SPQuad quadWithWidth:self.width height:self.height color:0x00DDAA];
        debugMarker.pivotY = debugMarker.height/2.0f;
        debugMarker.pivotX = debugMarker.width/2.0f;
        debugMarker.alpha = 0.7f;
        [self addChild:debugMarker];
    }
    [self flatten];
}
- (void)hideDenugMarker
{
    [self unflatten];
    [debugMarker removeFromParent];
    debugMarker = nil;
    [self flatten];
}
- (void)testCatchAnimation
{
    [super testCatchAnimation];
    SPTween *tewen = [SPTween tweenWithTarget:self time:0.1 transition:SP_TRANSITION_EASE_IN_OUT];
    [tewen animateProperty:@"scaleX" targetValue:1.5];
    [tewen animateProperty:@"scaleY" targetValue:1.5];
    [self.game.juggler addObject:tewen];
    
    SPTween *tween2 = [SPTween tweenWithTarget:self time:0.1 transition:SP_TRANSITION_EASE_OUT_IN];
    tween2.delay = tewen.totalTime;
    [tween2 animateProperty:@"scaleX" targetValue:1];
    [tween2 animateProperty:@"scaleY" targetValue:1];
    [self.game.juggler addObject:tween2];

}
@end

