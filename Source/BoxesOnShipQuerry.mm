//
//  BoxesOnShipQuerry.cpp
//  Save the ship!
//
//  Created by Piotrek on 05.07.2013.
//
//
#include "Box2D.h"
#include "CargoBox.h"
class BoxesOnShipQuerry : public b2QueryCallback {
public:
    int numberofBoxes;
    BoxesOnShipQuerry() : b2QueryCallback()
    {
        
        numberofBoxes = 0;
    }
    int RaportNumber()
    {
        return numberofBoxes;
    }
    bool ReportFixture(b2Fixture* fixture) {
        NSObject* obj = (__bridge NSObject *)fixture->GetBody()->GetUserData();
        if ([obj isKindOfClass:[CargoBox class]]) {
            numberofBoxes++;
        }
//        // Return true to continue the query.
////        printf("dupa");
        return true;
    }
};