//
//  Game.m
//  AppScaffold
//
#import "Game.h"
#import "MainGameStage.h"
#import "ColectableObject.h"
#import "BoxesOnShipQuerry.mm"

#define kAccelerometerFrequency        50.0 //Hz
#define kFilteringFactor 0.9
#define BUOYANCYOFFSET 140.0f
#define STARTPOINT 180.0f
#define BRUSHSIZE 50
@implementation Game
@synthesize physTimer, motionTimer, rootGameStage, gameContainer, backgroundContainer, GUIContainer;
- (void)setPunkty:(float)punkty
{
    _punkty = punkty;
    punktyTextField.text = [NSString stringWithFormat:@"%d", (int)punkty];
}
- (void)setCombo:(int)combo
{
    if (combo>1 && combo>_combo) {
//        NSLog(@"Combo! %d", combo);

        if (comboTextField==nil) {
            comboTextField = [SPTextField textFieldWithText:nil];
            
            comboTextField.color = 0xFFFFFF;
            comboTextField.x = [Sparrow stage].width/2;
            comboTextField.y =20;
            
//            comboTextField.alpha = 0;
//            SPTween *tween = [SPTween tweenWithTarget:comboTextField time:0.5f transition:SP_TRANSITION_EASE_IN_ELASTIC];
//            [tween fadeTo:1];
//            [self.juggler addObject:tween];
            [GUIContainer addChild:comboTextField];
        }

        comboTextField.text = [NSString stringWithFormat:@"Tower combo X%d!", combo];
    }else if(combo<=1)
    {
//        SPTween *tween = [SPTween tweenWithTarget:comboTextField time:0.5f transition:SP_TRANSITION_EASE_OUT_ELASTIC];
//        [tween fadeTo:0];
//        [tween addEventListenerForType:SP_EVENT_TYPE_REMOVE_FROM_JUGGLER block:^(id event){
            [comboTextField removeFromParent];
            comboTextField = nil;
//        }];
    }
    _combo = combo;

}
#pragma mark - Graphics
- (id)initWithWidth:(float)width height:(float)height andGameMode: (gameMode)mode andLevel: (int)lev
{
    if ((self = [super init]))
    {
        self.gameOverBool = NO;
//        self.alpha = 0.999;
        endgame = NO;
        orientation = [[UIApplication sharedApplication] statusBarOrientation];
        
        self.atlas = [SPTextureAtlas atlasWithContentsOfFile:@"atlas.xml"];
        self.juggler = [SPJuggler juggler];
        
        self.level = [Level levelClassForGameMode:mode andLevel:lev];
        self.level.game = self;
        gWidth = width;
        gHeight = height;
        offset = new long long;
        
        *offset = 0;
        floatigBodies = [[NSMutableArray alloc] init];
        
        backgroundContainer = [[SPSprite alloc] init];
        backgroundContainer.pivotX = gameContainer.width/2;
        backgroundContainer.pivotY = gameContainer.height/2;
        [self addChild:backgroundContainer];
        
        gameContainer = [[SPSprite alloc]init];
        gameContainer.pivotX = Sparrow.stage.width/2;
        gameContainer.pivotY = Sparrow.stage.height;
        gameContainer.x=Sparrow.stage.width/2;
        gameContainer.y=Sparrow.stage.height;
        
        [self addChild:gameContainer];
        
        
        
        
        GUIContainer = [[SPSprite alloc] init];
        [self addChild:GUIContainer];
        
        self.GameGUI = [[SPSprite alloc]init];
        
        self.GameGUI.pivotX = Sparrow.stage.width/2;
        self.GameGUI.pivotY = Sparrow.stage.height;
        self.GameGUI.x=Sparrow.stage.width/2;
        self.GameGUI.y=Sparrow.stage.height;
        [self addChild:self.GameGUI];
        
        touchZone = [SPQuad quadWithWidth:[Sparrow stage].width height:[Sparrow stage].height];
//        touchZone.alpha = 1.0;
//        touchZone.color = 0xFF00FF;
        touchZone.alpha = 0;
//        touchZone.visible = NO;
        [GUIContainer addChild:touchZone];
        
        
        pauzaButton= [SPButton buttonWithUpState:[self.atlas textureByName:@"pauza"]];
        [pauzaButton addEventListener:@selector(pauseEvent:) atObject:self forType:SP_EVENT_TYPE_TRIGGERED];
        pauzaButton.x = gWidth-pauzaButton.width;
        [GUIContainer addChild:pauzaButton];
        
        
        NieboTlo *niebot = [[NieboTlo alloc] initWithGame: self];
        [backgroundContainer addChild:niebot];
        
        
        [self createPhysicsWorldWithWidth:gWidth height:gHeight];
        
        self.statek = [[Statek alloc] initWithWorld:_world andGame:self];
        
        [self.statek initPhisicsWithPosision:b2Vec2(STARTPOINT/PTM_RATIO, (gHeight-200)/PTM_RATIO)];
        
        
        self.fala = [[Fala alloc] initWithWidth:self.height height:self.width];
        self.fala.game = self;
        self.fala.validate = YES;
        [self.fala configure];
        
        //        self.level.predkoscFalowania = 6;
        
        self.fala.offset = offset;
        
        
        //Woda
        
        [self addEventListener:@selector(onEnterFrame:) atObject:self forType:SP_EVENT_TYPE_ENTER_FRAME];
        
        monety = [NSMutableArray array];
        explosionLayer = [SPSprite sprite];
        [gameContainer addChild:explosionLayer];
        
        [gameContainer addChild:self.fala];
        [gameContainer addChild:self.fala.falaMask];
        
        
        [gameContainer addChild:self.statek];
        
        bombLayer = [SPSprite sprite];
        [gameContainer addChild:bombLayer];
        [self drawBox];
        
        [gameContainer addChild:self.fala.falaRender];
        
        
        
        
        self.physTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f/30.0f target:self selector:@selector(bortInfo:) userInfo:nil repeats:YES];
        
        [self configureAccelerometer];
        
        
        [touchZone addEventListener:@selector(onTouch:) atObject:self forType:SP_EVENT_TYPE_TOUCH];
        
        uint32 flags = 0;
        flags += b2Draw::e_shapeBit;
        flags += b2Draw::e_jointBit;
        //        flags += b2Draw::e_aabbBit;
        flags += b2Draw::e_pairBit;
        flags += b2Draw::e_centerOfMassBit;
        //MARK: DebugDraw
        //        debuf = [BoxDebug debugLayerWithWorld:_world ptmRatio:PTM_RATIO flags:flags];
        //        [gameContainer addChild:debuf];
        
        if (self.level.levelNumber!=0&& self.level.levelNumber!=2) {
            SXParticleSystem *deszcz = [SXParticleSystem particleSystemWithContentsOfFile:@"deszcz.pex"];
            deszcz.scaleY=-1;
            deszcz.alpha = 0.5;
            [backgroundContainer addChild:deszcz];
            [deszcz start];
            [self.juggler addObject:deszcz];

        }
        if (self.level.levelNumber==2)
        {

        }
        
        if(!self.level.continues)
        {
            info = [[EndLevelInfo alloc]init];
            info.game = self;
            info.x  = self.level.gameWidth-STARTPOINT-181;
            [self.GameGUI addChild:info];
            
            SPImage *wyspa = [SPImage imageWithContentsOfFile:@"wyspa.png"];
            
            [self.gameContainer addChild:wyspa atIndex:0];
            wyspa.x = self.level.gameWidth - STARTPOINT - 240 ;
        }
        
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(orientationChanged:)
                                                     name:@"UIApplicationDidChangeStatusBarOrientationNotification"
                                                   object:nil];    }
    
    
    punktyTextField = [SPTextField textFieldWithText:@"0000"];
    punktyTextField.color = 0xFFFFFF;
    punktyTextField.x = 10;
    punktyTextField.touchable = NO;
    punktyTextField.fontSize = 20;
    punktyTextField.y = [Sparrow stage].height - 100;
    [self.GUIContainer addChild:punktyTextField];
    [self.level startLevel];
    
    

    SPSound *sound;
    if (self.level.levelNumber==0) {
        sound = [SPSound soundWithContentsOfFile:@"miasto-export.mp3" ];
    }else if(self.level.levelNumber==2)
    {
        sound = [SPSound soundWithContentsOfFile:@"arktik(remix).mp3" ];
    }
    else
    {
        sound = [SPSound soundWithContentsOfFile:@"Burza-export.mp3" ];
    }
   

    backgoundMusic = [sound createChannel];
    [backgoundMusic play];
    backgoundMusic.loop = YES;
    
    SPSound *tloM = [SPSound soundWithContentsOfFile:@"Morze-Burza.mp3"];
    backgoundNoise = [tloM createChannel];
    backgoundNoise.loop = YES;
    backgoundNoise.volume = 0.2;
    [backgoundNoise play];
    return self;
}
- (void)strataSkrzyni
{
    
}
- (void)onTouch: (SPTouchEvent *)event
{
    if (!endgame)
    {
        
        //    NSLog(@"boxes: %d", [self boxesOnTheShip]);
        NSSet *set = [event touchesWithTarget:touchZone andPhase:SPTouchPhaseMoved];
        if (set.count>0) {
            for (FloatingBody *fBody in floatigBodies) {
                fBody.draging = false;
                
                SPTouch *touch = [set anyObject];
                SPPoint *lastTouchPoint = [touch previousLocationInSpace:self.gameContainer];
                SPPoint *touchPoint = [touch locationInSpace:self.gameContainer];
                SPPoint *center = [SPPoint pointWithX:fBody.x y:fBody.y];
                //            NSLog(@"%@", lastTouchPoint);
                float sila = 3;
                if ([SPPoint distanceFromPoint:lastTouchPoint toPoint:center] < BRUSHSIZE ) {
                    SPPoint *silaPrzesuniecia = [SPPoint point];
                    silaPrzesuniecia.x = (touchPoint.x-lastTouchPoint.x);
                    silaPrzesuniecia.y = (touchPoint.y-lastTouchPoint.y);
                    [silaPrzesuniecia normalize];
                    int maxforce = 100;
                    if (silaPrzesuniecia.x>maxforce) {
                        silaPrzesuniecia.x=maxforce;
                    }
                    if (silaPrzesuniecia.x<-maxforce) {
                        silaPrzesuniecia.x=-maxforce;
                    }
                    
                    if (silaPrzesuniecia.y>maxforce) {
                        silaPrzesuniecia.y=maxforce;
                    }
                    if (silaPrzesuniecia.y<-maxforce) {
                        silaPrzesuniecia.y=-maxforce;
                    }
                    
                    fBody->body->ApplyForce(b2Vec2(
                                                   silaPrzesuniecia.x*fBody->body->GetMass()*sila,
                                                   -silaPrzesuniecia.y*fBody->body->GetMass()*sila),
                                            fBody->body->GetWorldCenter());
                    
                    
                    
                    
                    if (!fBody.draging) {
                    }
                    if ([fBody isKindOfClass:[CargoBox class]]&& ((CargoBox *)fBody).niezatapialny) {
                        [((CargoBox *)fBody) removeKolo];
                        self.level.skrzynkiZKolem--;
                    }
                    fBody.draging = true;
                    
                }
            }
        }
        
    }
}
- (void)pauseEvent: (SPEvent *)event
{
    [self.rootGameStage pauseGame];
}
- (void)pause
{
    [pauzaButton removeEventListenersAtObject:self forType:SP_EVENT_TYPE_TRIGGERED];
    
    [self.physTimer invalidate];
    
    [self.motionTimer invalidate];
    self.fala.validate = NO;
    self.GUIContainer.visible = NO;
//    [self flatten];
}
- (void)gameQuit
{
    
}
- (void)unPause
{
//    [self unflatten];

    [pauzaButton addEventListener:@selector(pauseEvent:) atObject:self forType:SP_EVENT_TYPE_TRIGGERED];
    
    self.physTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f/30.0f target:self selector:@selector(bortInfo:) userInfo:nil repeats:YES];
    
    self.motionTimer = [NSTimer scheduledTimerWithTimeInterval:[motionManager deviceMotionUpdateInterval] target:self selector:@selector(accelerometrUpdate) userInfo:nil repeats:YES];
    self.fala.validate = YES;
    self.GUIContainer.visible = YES;
}
- (void)onEnterFrame: (SPEnterFrameEvent *)event
{
    int C=0;
    
    int najwyzszyElement = 0;
    GunpowerBarrel *ebj;
    for (FloatingBody *obj in floatigBodies) {
        
        if ([obj isKindOfClass:[CargoBox class]]) {
            ((CargoBox *)obj).emitter.emitterX = obj.x;
            ((CargoBox *)obj).emitter.emitterY = -obj.y;
            
            
            int wys = Sparrow.stage.height-obj.y;
            if (najwyzszyElement<wys) {
                najwyzszyElement = wys;
            }
            if (obj.y>[Sparrow.stage height]*1.2) {
                [((CargoBox *)obj) wyjsciePozaZakres];
            }
            if (C<((CargoBox *)obj).towerLevel) {
                C = ((CargoBox *)obj).towerLevel;
            }
        }else if ([obj isKindOfClass:[GunpowerBarrel class]])
        {
            if (obj.x<-*offset*gameContainer.scaleX+2*Sparrow.stage.width) {
                ebj = ((GunpowerBarrel *)obj);
                [bombLayer addChild:ebj];
                [self.juggler addObject:ebj.system];
                [ebj addChild:ebj.system];
                
                [ebj.system start];
            }
        }
        
    }
    self.combo= C;
    int wys = Sparrow.stage.height-self.statek.y;
    if (najwyzszyElement<wys) {
        najwyzszyElement = wys;
    }
    
    //    for (int i=0; i<self.level.iloscBomb; i++) {
    //    }
    
    
    int requiredHeight = najwyzszyElement+60;
    //    NSLog(@"r: %f", requiredHeight);
    if (requiredHeight<320 || endgame) {
        requiredHeight = 320;
    }
    float scale = Sparrow.stage.height/requiredHeight;
    if (scale<0.5) {
        scale=0.5;
    }
    scaleTween = [SPTween tweenWithTarget:gameContainer time:.05f transition:SP_TRANSITION_EASE_IN];
    [scaleTween animateProperty:@"scaleX" targetValue:scale];
    [scaleTween animateProperty:@"scaleY" targetValue:scale];
    //    if (scaleTween) {
    //        [self.juggler removeObject:scaleTween];
    //    }
    //    [self.juggler addObject:scaleTween];
    gameContainer.scaleY = scale;
    gameContainer.scaleX = scale;
    //    [self SpotionalScale:gameContainer :scale];
    
    
    alfa = alfa+0.05f;
    float dzielnik = 2*M_PI;
    
    alfa = fmodf(alfa, dzielnik);
    self.fala.frameRate = event.passedTime;
    if ((self.statek.x+STARTPOINT<self.level.gameWidth || self.level.continues) && !self.gameOverBool) {
        if(-self.statek.x+STARTPOINT>0)
        {
            *offset = 0;
            camx = 0;
            gameContainer.x = Sparrow.stage.width/2;
            self.GameGUI.x = Sparrow.stage.width/2;
        }
        else
        {
            *offset = -self.statek.x+STARTPOINT;
            camx = -self.statek.x+STARTPOINT;
            gameContainer.x = *offset*gameContainer.scaleX+Sparrow.stage.width/2;
            self.GameGUI.x = *offset+Sparrow.stage.width/2;
            
        }
        
    }else if(!endgame)
    {
        if (self.statek.x+Sparrow.stage.width/2>*offset) {
            //            *offset = -self.statek.x+Sparrow.stage.width/2;
            //            camx = -self.statek.x+Sparrow.stage.width/2;
        }
        gameContainer.x = *offset*gameContainer.scaleX+Sparrow.stage.width/2;
        self.GameGUI.x = *offset+Sparrow.stage.width/2;
        
        [self levelEnd];
    }else
    {
        if (self.statek.x+Sparrow.stage.width/2>*offset) {
            //            *offset = -self.statek.x+Sparrow.stage.width/2;
            //            camx = -self.statek.x+Sparrow.stage.width/2;
        }
        gameContainer.x = *offset*gameContainer.scaleX+Sparrow.stage.width/2;
        self.GameGUI.x = *offset+Sparrow.stage.width/2;
        
    }
    //
    for (ColectableObject *moneta in monety) {
        if (!moneta.colected) {
            for (FloatingBody *body in floatigBodies) {
                if ([body isKindOfClass:CargoBox.class]) {
                    SPRectangle *bounds1 = body.bounds;
                    SPRectangle *bounds2 = moneta.bounds;
                    
                    if ([bounds1 intersectsRectangle:bounds2])
                    {
                        self.punkty += 10;
                        [(CargoBox *)body testCatchAnimation];
                        moneta.colected = YES;
                        moneta.destination = nil;
                        [self.juggler removeObject:moneta.tween];
                        moneta.alpha = 0;
//                        [moneta removeFromParent];
                        SPSound *sound = [SPSound soundWithContentsOfFile:@"Moneta 2.mp3" ];
                        
                        moneta.channel = [sound createChannel];
                        [moneta.channel play];
                        moneta.channel.loop = NO;

                    }else if ([SPPoint distanceFromPoint:[SPPoint pointWithX:body.x y:body.y] toPoint:[SPPoint pointWithX:moneta.x y:moneta.y]]<100)
                    {
                        if (moneta.tween==nil) {
                            [moneta startFolowing:(CargoBox *)body];
                        }
                        
                    }
                }
            }
            
        }
    }
    
    
    self.punkty += self.level.iloscSkrzyn*0.001*self.statek.szybkoscStatku;
    
    //    if (statek.y<gHeight*1/3) {
    //        gameContainer.y = gHeight*1/3-statek.y;
    //
    //    }else
    //    {
    //        gameContainer.y = 0;
    //    }
    
//    self.statek.szybkoscStatku = self.level.predkoscPlywania*(1/(arc4random()%40+1)+1);
}
- (void)gameOver
{
    NSLog(@"Game Over");
    touchZone.touchable = NO;
    //    self.level.pet la = 2;
    SPTween *tween = [SPTween tweenWithTarget:self.statek time:1.8 transition:SP_TRANSITION_EASE_OUT];
    [tween animateProperty:@"szybkoscStatku" targetValue:0];
    [tween addEventListenerForType:SP_EVENT_TYPE_REMOVE_FROM_JUGGLER block:^(id event){
        [self.level zeroSpeed];
    }];
    [self.juggler removeObjectsWithTarget:self.statek];
    [self.juggler addObject:tween];
    gameContainer.x = *offset*gameContainer.scaleX+Sparrow.stage.width/2;
    self.GameGUI.x = *offset+Sparrow.stage.width/2;
    
}
- (void)levelEnd
{
    NSLog(@"Level Over");
    endgame = YES;
    touchZone.touchable = NO;
    //    self.level.pet la = 2;
    SPTween *tween = [SPTween tweenWithTarget:self.statek time:1.8 transition:SP_TRANSITION_EASE_OUT];
    [tween animateProperty:@"szybkoscStatku" targetValue:0];
    [tween addEventListenerForType:SP_EVENT_TYPE_REMOVE_FROM_JUGGLER block:^(id event){
        [self.level zeroSpeed];
    }];
    [info finish];
    
    [self.juggler removeObjectsWithTarget:self.statek];
    [self.juggler addObject:tween];
    gameContainer.x = *offset*gameContainer.scaleX+Sparrow.stage.width/2;
    self.GameGUI.x = *offset+Sparrow.stage.width/2;
}
- (void)end
{
    
    delete _world;
    self.rootGameStage = nil;
    motionManager = nil;
    [motionManager stopAccelerometerUpdates];
    [self.motionTimer invalidate];
    [self.physTimer invalidate];
}
- (void)dealloc
{
    [backgoundMusic stop];
    
    NSLog(@"Dealloc Game");
}
#pragma mark - Physics
- (void)bortInfo:(NSTimer*)theTimer
{
    
    [self.statek speedControl];
    
    [self.fala step];
    float fixedTimeStep = 1.0/60.0f;
    float timeToRun = theTimer.timeInterval + timeAccumulated;
    //It is recommended that a fixed time step is used with Box2D for stability
    //of the simulation, however, we are using a variable time step here.
    //You need to make an informed choice, the following URL is useful
    //http://gafferongames.com/game-physics/fix-your-timestep/
    
    int32 velocityIterations = 8;
    int32 positionIterations = 1;
    
    
    while(timeToRun >= fixedTimeStep) {
        //        [self destroyFloatingBodies];
        _world->Step(fixedTimeStep, velocityIterations, positionIterations);
        
        //Iterate over the bodies in the physics world
        for (b2Body* b = _world->GetBodyList(); b; b = b->GetNext())
        {
            if (b->GetUserData() != NULL) {
                
                
                //Synchronize the AtlasSprites position and rotation with the corresponding body
                SPSprite *myActor = (__bridge SPSprite*)b->GetUserData();
                
                myActor.x = b->GetPosition().x * PTM_RATIO;
                // Remember that Y-axis directions in Sparrow and Box2D are opposite
                myActor.y = gHeight - b->GetPosition().y * PTM_RATIO;
                myActor.rotation = -1 * b->GetAngle();
                if ([myActor isKindOfClass:[FloatingBody class]]) {
                    
                    if (((FloatingBody *)myActor).destroyed ) {
                        if ([myActor isKindOfClass:[CargoBox class]]) {
                            [self.level wywalonaSkrzynia];
                        }
                        _world->DestroyBody(b);
                        [myActor removeFromParent];
                        
                        
                    }else
                    {
                        [self.fala stepWithBody:b];
                        
                    }
                    
                }
                
                if ([myActor respondsToSelector:@selector(executePhysics)])
                {
                    [myActor performSelector:@selector(executePhysics)];
                }
                
            }
        }
        
        timeToRun -= fixedTimeStep;
    }
    
    timeAccumulated = timeToRun;
}
-(void)createPhysicsWorldWithWidth:(float)width height:(float)height
{
    b2Vec2 gravity;
    gravity.Set(0.0f, -10.0f);
    _world = new b2World(gravity);
    
    _world->SetAllowSleeping(YES);
    _world->SetContinuousPhysics(true);
    //    _world->set
    myContactListenerInstance = new myContactListiner;
    _world->SetContactListener(myContactListenerInstance);
    
    
    
    //////
    
    
}

- (void)drawBox
{
    //TODO: przenieść do LevelClass
    SPSprite *particleEfects = [SPSprite sprite];
    for (int i=0; i<self.level.iloscSkrzyn; i++) {
        CargoBox *cargo1 = [[CargoBox alloc] initWithWorld:_world andGame:self];
        cargo1.offset = offset;
        [self.juggler addObject:cargo1.emitter];
        [particleEfects addChild:cargo1.emitter];
        [cargo1.emitter start];
        cargo1.emitter.delegate = self.fala;
        cargo1.floatingBodies = floatigBodies;
        
        [gameContainer addChild:cargo1];
        [floatigBodies addObject:cargo1];
        
    }
    for (SPPoint *point in self.level.pakietyPozycje) {
        for (int i=0; i<self.level.PakietIloscW; i++) {
            for (int k=0; k<self.level.PakietIloscH ; k++) {
                ColectableObject *moneta = [[ColectableObject alloc]init];
                moneta.x = point.x+20*i;
                moneta.y = point.y+20*k;
                [gameContainer addChild:moneta];
                [monety addObject:moneta];
            }
        }
        
    }
    GunpowerBarrel *barrel;
    for (int i=0; i<self.level.iloscBomb; i++) {
        barrel = [[GunpowerBarrel alloc] initWithWorld:_world andGame:self];
        barrel->body->SetTransform(b2Vec2(self.level.bombs[i].x/PTM_RATIO, self.level.bombs[i].y/PTM_RATIO), 0);
        [floatigBodies addObject:barrel];
        barrel.floatingBodies = floatigBodies;
        [gameContainer addChild:barrel];
    }
    [self.gameContainer addChild:particleEfects];
}
- (void)explosionOnPoint:(SPPoint *)point
{
    //Cząsteczki
    
    SXParticleSystem* explosionEmiter = [[SXParticleSystem alloc] initWithContentsOfFile:@"wybuch.pex"];
    explosionEmiter.scaleY=-1;
    explosionEmiter.scaleX=1;
    explosionEmiter.emitterX = point.x;
    explosionEmiter.emitterY = -point.y;
    
    [explosionLayer addChild:explosionEmiter];
    [self.juggler addObject:explosionEmiter];
    
    [explosionEmiter startBurst:0.2];
    __weak SXParticleSystem* blockeEmiter = explosionEmiter;
    [explosionEmiter addEventListenerForType:@"complete" block:^(id event)
     {
         [self.juggler removeObject:blockeEmiter];
         
     }];
    //    self.fala.explosionEmiter = self.explosionEmiter;
    
}
- (int)boxesOnTheShip
{
    SPQuad *quad = [SPQuad quadWithWidth:100 height:100 color:0xFF00AA];
    quad.x = 200;
    quad.y = 100;
    quad.pivotX = quad.width/2.0f;
    quad.pivotY = quad.height/2.0f;
    [self.gameContainer addChild:quad];
    BoxesOnShipQuerry callback;
    b2AABB aabb;
    aabb.lowerBound.Set(
                        (quad.pivotX-quad.width/2.f)/PTM_RATIO,
                        (0)/PTM_RATIO);
    aabb.upperBound.Set(
                        (quad.pivotX+quad.width/2.f)/PTM_RATIO,
                        (320.f)/PTM_RATIO);
    _world->QueryAABB(&callback, aabb);
    
    return callback.numberofBoxes;
}
#pragma mark - Accelerometr
- (void)orientationChanged:(NSNotification *)notification{
    orientation = [[UIApplication sharedApplication] statusBarOrientation];
}
- (void)accelerometrUpdate
{
    xf = kFilteringFactor * xf + (1.0 - kFilteringFactor) * motionManager.accelerometerData.acceleration.x;
    yf = kFilteringFactor * yf + (1.0 - kFilteringFactor) * motionManager.accelerometerData.acceleration.y;
    if (xf>-1) {
        xf=-1;
    }
    float angle;
    angle = atan2(yf, xf);
    
    
    if (angle>M_PI) {
        angle=M_PI;
    }
    if (self.statek.bodyPhys != nil) {
        angle = M_PI-angle;
        
        if (angle>M_PI) {
            angle  = M_PI*2-angle;
            
            angle = angle*-1;
        }
        if (orientation == 4) {
            
            angle = -angle;
        }
        self.statek.accRotation = angle;
        [self.statek calculateRoation];
    }
    
}
-(void)configureAccelerometer
{
    motionManager = [[CMMotionManager alloc]init];
    
    
    motionManager.deviceMotionUpdateInterval = 1.0f/30.0f;
    if (!motionManager.accelerometerAvailable) {
        NSLog(@"accelerometr unavalible");
    }
    [motionManager startAccelerometerUpdates];
    memset(filteredAcceleration, 0, sizeof(filteredAcceleration));
    
    self.motionTimer = [NSTimer scheduledTimerWithTimeInterval:[motionManager deviceMotionUpdateInterval] target:self selector:@selector(accelerometrUpdate) userInfo:nil repeats:YES];
    
}
@end
