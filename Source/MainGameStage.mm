//
//  MainGameStage.m
//  Save the ship!
//
//  Created by Piotrek on 10.12.2012.
//
//

#import "MainGameStage.h"
#import "Game.h"
#import "Menu.h"
#import "PauseMenu.h"
#import "GameMap.h"
#import "LevelParser.h"
@implementation MainGameStage
@synthesize delegate;
- (id)init
{
    if ((self = [super init])) {
        NSLog(@"Start");
//        SPQuad *quad = [SPQuad quadWithWidth:<#(float)#> height:<#(float)#>]
        
        
//        UIInterfaceOrientation orientation = [self initialInterfaceOrientation];
        float width = Sparrow.stage.width;
        float height = Sparrow.stage.height;
        NSLog(@"%f, %f", width, height);

//        if (UIInterfaceOrientationIsLandscape(orientation)) SP_SWAP(width, height, int);
        
        self.gameHeight = height;
        self.gameWidth = width;
        
        mainGameContainer = [SPSprite sprite];

        mainGameContainer.width = width;
        mainGameContainer.height = height;
//
        mainGameContainer.x = width/2;
        mainGameContainer.y = height/2;
        mainGameContainer.pivotX = width/2;
        mainGameContainer.pivotY = height/2;
        
        mainGameContainer.rotation = 0;

        [self addChild:mainGameContainer];
//        [self rotateToInterfaceOrientation:orientation animationTime:0];

        //    [((Game *)mSparrowView.stage) gamePause:event];
        [self backToMenu];
        
        [self addEventListener:@selector(onEnterFrame:) atObject:self
                       forType:SP_EVENT_TYPE_ENTER_FRAME];
        pauza = Nil;

    }
    return  self;
}
- (void)onEnterFrame:(SPEnterFrameEvent *)event
{
    if (!pauza) {
        [game.juggler advanceTime:event.passedTime];
    }
}
#pragma mark Game View Controller Methods

- (void)startTestGame
{
    game = [[Game alloc]initWithWidth: Sparrow.stage.width
                                     height: Sparrow.stage.height
                                andGameMode:gameModeLevel andLevel:0];
//    game.rotation = M_PI_2;
//    game.x = self.width;
    game.rootGameStage  = self;
    [mainGameContainer removeAllChildren];
    [mainGameContainer addChild:game];

    NSLog(@"start game");
}
- (void)startLevel: (int)lev
{
    [game end];
    game = nil;
    game = [[Game alloc]initWithWidth: Sparrow.stage.width
                               height: Sparrow.stage.height
                          andGameMode:gameModeLevel andLevel:lev];
    //    game.rotation = M_PI_2;
    //    game.x = self.width;
    game.rootGameStage  = self;
    [mainGameContainer removeAllChildren];
    [mainGameContainer addChild:game];
    
    NSLog(@"start game");

}
- (void)pauseGame
{
    if (pauza==nil && game) {
        pauza = [[PauseMenu alloc]init];
        pauza.rootGameStage = self;
        [pauza setup];
        [mainGameContainer addChild:pauza];

        if (game) {
            [game pause];
        }
    }

}
- (void)unpauseGame
{
    if (pauza!=nil && game) {
        if (game) {
            [game unPause];
        }
        [mainGameContainer removeChild:pauza];
        pauza = nil;
    }
}
- (void)startGameWithOptions
{
    
}
- (void)backToMenu
{
    Menu *menu = [[Menu alloc]init];
    menu.rootGameStage = self;
    //        menu = [menu init];
    [menu setup];
    [mainGameContainer removeAllChildren];
    
    [mainGameContainer addChild:menu];
    pauza = Nil;
    [game end];
    game = nil;
    
}
- (void)leaveGameWithOptions
{
    
}
- (void)showMap
{
    GameMap *mapa = [[GameMap alloc]init];
    mapa.rootGameStage = self;
    //        menu = [menu init];
//    [mapa setup];
    [mainGameContainer removeAllChildren];
    
    [mainGameContainer addChild:mapa];
    pauza = Nil;

}
@end