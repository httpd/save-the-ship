//
//  ColectableObject.h
//  Save the ship!
//
//  Created by Piotrek on 17.04.2013.
//
//

#import "SPDisplayObject.h"
@class CargoBox;
@interface ColectableObject : SPDisplayObjectContainer
@property BOOL colected;
@property (atomic, strong) SPTween *tween;
@property (strong) CargoBox *destination;
@property float animationCompletion;
@property SPSoundChannel *channel;
- (void)startFolowing: (CargoBox *)obiekt;
@end
