//
//  Menu.m
//  Save the ship!
//
//  Created by Piotrek on 17.09.2012.
//
//

#import "Menu.h"
#import "MainGameStage.h"
@implementation Menu
@synthesize rootGameStage;
- (id)init
{
    if ((self = [super init])) {

    }
    return  self;
}
- (void)setup
{
    NSLog(@"run");
    
    SPQuad *background = [SPQuad quadWithWidth:Sparrow.stage.width height:Sparrow.stage.height color:0x00DD00];
    [self addChild:background];
    SPButton *start = [SPButton buttonWithUpState:[SPTexture textureWithContentsOfFile:@"play.png"]];
    start.pivotX = start.width/2;
    start.pivotY = start.height/2;
    start.x = [Sparrow stage].width/2;
    start.y = [Sparrow stage].height/2;
    [start addEventListener:@selector(openMap:) atObject:self forType:SP_EVENT_TYPE_TRIGGERED];
    [self addChild:start];
    
//    SPQuad *mapa = [SPButton buttonWithUpState:[SPTexture textureWithContentsOfFile:@"start.png"] text:@"Mapa"];
//    mapa.x = 50;
//    mapa.y = 50;
//    [mapa addEventListener:@selector(openMap:) atObject:self forType:SP_EVENT_TYPE_TRIGGERED];
//    [self addChild:mapa];
    

}
- (void)openMap: (SPTouchEvent *)event
{
        [self.rootGameStage showMap];
}
- (void)startClicked: (SPEvent  *)event
{
    
    [self.rootGameStage startTestGame];
}
@end
