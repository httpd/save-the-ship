//
//  EndLevelInfo.m
//  Save the ship!
//
//  Created by Piotrek on 23.07.2013.
//
//

#import "EndLevelInfo.h"
#import "Game.h"
#import "MainGameStage.h"
@implementation EndLevelInfo
- (id)init
{
    self = [super init];
    if (self) {
//        SPQuad *plhd = [SPQuad quadWithWidth:[Sparrow stage].width height:Sparrow.stage.height color:0x0F0F0F];
//        plhd.alpha = 0.5f;
//        [self addChild:plhd];
        self.touchable = YES;
        SPImage *star1 = [SPImage imageWithContentsOfFile:@"star1.png"];
        star1.x = [Sparrow stage].width/2;
        star1.y = 40;
        star1.pivotX = star1.width/2;
        star1.pivotY = star1.height/2;
        [self addChild:star1];
        
        SPImage *star2 = [SPImage imageWithContentsOfFile:@"star1.png"];
        star2.x = [Sparrow stage].width/2-60;
        star2.y = 45;
        star2.pivotY = star2.height/2;
        star2.pivotX = star2.width/2;
        [self addChild:star2];
        
        SPImage *star3 = [SPImage imageWithContentsOfFile:@"star1.png"];
        star3.x = [Sparrow stage].width/2+60;
        star3.y = 45;
        star3.pivotX = star3.width/2;
        star3.pivotY = star3.height/2;
        [self addChild:star3];

    }
    return self;
}

- (void)finish
{
    float t = 2.5f;
    
    SPButton *strzala = [SPButton buttonWithUpState:[SPTexture textureWithContentsOfFile:@"strzala.png"]];
    strzala.x = [Sparrow stage].width+60;
    strzala.y = 250;
    strzala.pivotX = strzala.width/2;
    strzala.pivotY = strzala.height/2;
    [self addChild:strzala];

    SPTween *animacjaStrzaly = [SPTween tweenWithTarget:strzala time:t transition:SP_TRANSITION_EASE_OUT_ELASTIC];
    [animacjaStrzaly animateProperty:@"x" targetValue:[Sparrow stage].width/2+120];
    [self.game.juggler addObject:animacjaStrzaly];
    
    [strzala addEventListener:@selector(goToNextLevel) atObject:self forType:SP_EVENT_TYPE_TRIGGERED];
    SPButton *repat = [SPButton buttonWithUpState:[SPTexture textureWithContentsOfFile:@"repeat.png"]];
    repat.x = -60;
    repat.y = 250;
    repat.pivotX = repat.width/2;
    repat.pivotY = repat.height/2;
    [self addChild:repat];
    
    [repat addEventListener:@selector(repeatGame) atObject:self forType:SP_EVENT_TYPE_TRIGGERED];
    SPTween *animacjaRepat = [SPTween tweenWithTarget:repat time:t transition:SP_TRANSITION_EASE_OUT_ELASTIC];
    [animacjaRepat animateProperty:@"x" targetValue:[Sparrow stage].width/2-120];
    [self.game.juggler addObject:animacjaRepat];
    
    SPButton *levelSelect = [SPButton buttonWithUpState:[SPTexture textureWithContentsOfFile:@"levelSelect.png"]];
    levelSelect.x = [Sparrow stage].width/2;
    levelSelect.y = [Sparrow stage].height+40;
    levelSelect.pivotX = levelSelect.width/2;
    levelSelect.pivotY = levelSelect.height/2;
    [self addChild:levelSelect];
    [levelSelect addEventListener:@selector(goBackToMenu) atObject:self forType:SP_EVENT_TYPE_TRIGGERED];
    SPTween *animacjaLev = [SPTween tweenWithTarget:levelSelect time:t transition:SP_TRANSITION_EASE_OUT_ELASTIC];
    [animacjaLev animateProperty:@"y" targetValue:250];
    [self.game.juggler addObject:animacjaLev];


}
- (void)repeatGame
{
    NSLog(@"repat");
    [self.game.rootGameStage startLevel:self.game.level.levelNumber];

}
- (void)goBackToMenu
{
    NSLog(@"mapa");
    [self.game.rootGameStage showMap];

}
- (void)goToNextLevel
{
    NSLog(@"next");
    [self.game.rootGameStage startLevel:self.game.level.levelNumber+1];

}
@end
