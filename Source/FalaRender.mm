
//  FalaRender.m
//  Save the ship!
//
//  Created by Piotrek Knapczyk on 24.07.2012.
//  Copyright (c) 2012 IdeaStudio. All rights reserved.
//

#import "FalaRender.h"
#import <GLKit/GLKit.h>
@implementation FalaRender
@synthesize complete, lenght,offset;


- (void)setOffset:(long long *)eoffset
{
    offset = eoffset;
}
- (long long *)offset
{
    return offset;
}
- (SPRectangle*)boundsInSpace:(SPDisplayObject*)targetSpace
{
    SPMatrix *matrix = [self transformationMatrixToSpace:targetSpace];
    return [_vertexData boundsAfterTransformation:matrix];
}

- (id)initWithLevel: (int)lev
{
    self = [super init];
    if (self) {

        complete = YES;
        self.touchable = NO;
        texture = [SPTexture textureWithContentsOfFile:@"woda13.png" generateMipmaps:YES] ;

        if (lev==2) {
            texture = [SPTexture textureWithContentsOfFile:@"woda_13_arctic.png" generateMipmaps:YES] ;

        }
        texture.repeat = YES;
        texture.smoothing = SPTextureSmoothingBilinear;
        
        

    }
    return self;
}
- (void)setup
{
    self.baseEffect = [[SPBaseEffect alloc] init];
    self.vertexData = [[SPVertexData alloc] init];
    self.vertexData.numVertices = SECTIONS;
    [self.vertexData setColor:0x999999];
//    self.blendMode = SP_BLEND_MODE_NONE;
    self.vertexData.premultipliedAlpha = YES;


    [self.baseEffect setTexture:texture];
//    self.baseEffect.useTinting = YES;
//    [self.vertexData setAlpha:0.2];

    [self createBuffers];

}

- (void)createBuffers
{

    if (vertexBufferName) glDeleteBuffers(1, &vertexBufferName);
    
    glGenBuffers(1, &vertexBufferName);
    
    if (!vertexBufferName)
        [NSException raise:SP_EXC_OPERATION_FAILED format:@"could not create vertex buffers"];
    
    glBindBuffer(GL_ARRAY_BUFFER, vertexBufferName);
    glBufferData(GL_ARRAY_BUFFER, sizeof(SPVertex) * self.vertexData.numVertices, self.vertexData.vertices, GL_DYNAMIC_DRAW);

    int attribPosition = self.baseEffect.attribPosition;
    int attribTexCoords = self.baseEffect.attribTexCoords;
    int attribColor = self.baseEffect.attribColor;
    glEnableVertexAttribArray(attribPosition);
    glEnableVertexAttribArray(attribTexCoords);
    glEnableVertexAttribArray(attribColor);

}

- (void)resetThTable
{
    if (self.offset) {

        int begin = -(*self.offset)/10;
        
        float U_Off = floor(begin / 51.2f);
//        NSLog(@"%f",U_Off);
        for (int i = 0; i<SECTIONS/2.0f; i++) {
            
//            double przesuniecie = th[2*(i+begin)].y;
//            przesuniecie/=170;
//            NSLog(@"%f", przesuniecie);
//            texCoords[i*2+1] = CGPointMake(0.01*i+0.01*begin,1.0);
//            texCoords[i*2] = CGPointMake(0.01*i+0.01*begin, 0.0);
//            div_t divresult;
//            divresult = div (begin*step,512);


            
            float step = 0.02;
            float ptx = step*i+step*begin-U_Off;
            

            [self.vertexData setTexCoords:[SPPoint pointWithX:ptx y:1.0] atIndex:i*2+1];
            [self.vertexData setTexCoords:[SPPoint pointWithX:ptx y:0.005] atIndex:i*2];


//            secTexCoords[i*2+1] = CGPointMake(0.07*i+0.05*begin,1.0);
//            secTexCoords[i*2] = CGPointMake(0.07*i+0.05*begin, 0.0);

            
//            lineTexCoords[i] = texCoords[i*2+1];
        }

    }
//    NSLog(@"koniec");
}
- (void)render:(SPRenderSupport *)support
{
//    [super render:support];
    [self resetThTable];

    [self createBuffers];
//    [self.vertexData setColor:0xAAAAAA alpha:1];



    
    [support finishQuadBatch]; // finish previously batched quads
    [support addDrawCalls:1]; // update stats display

    
    self.baseEffect.mvpMatrix = support.mvpMatrix;
    self.baseEffect.alpha = support.alpha;
    [self.baseEffect prepareToDraw];
    

    [SPBlendMode applyBlendFactorsForBlendMode:support.blendMode premultipliedAlpha:self.vertexData.premultipliedAlpha];
    
    int attribPosition = self.baseEffect.attribPosition;
    int attribTexCoords = self.baseEffect.attribTexCoords;
    int attribColor = self.baseEffect.attribColor;    
    glEnableVertexAttribArray(attribPosition);
    glEnableVertexAttribArray(attribTexCoords);
    glEnableVertexAttribArray(attribColor);

//    glBindBuffer(GL_ARRAY_BUFFER, vertexBufferName);
    glBufferData(GL_ARRAY_BUFFER, sizeof(SPVertex) * self.vertexData.numVertices, self.vertexData.vertices, GL_STATIC_DRAW);

    
    glVertexAttribPointer(attribPosition, 2, GL_FLOAT, GL_FALSE, sizeof(SPVertex), (void *)(offsetof(SPVertex, position)));
    
    glVertexAttribPointer(attribTexCoords, 2,  GL_FLOAT, GL_FALSE, sizeof(SPVertex),(void *)(offsetof(SPVertex, texCoords)));

    glVertexAttribPointer(attribColor, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(SPVertex),(void *)(offsetof(SPVertex, color)));
    
    glDrawArrays(GL_TRIANGLE_STRIP, 0, self.vertexData.numVertices);

}

@end