//
//  AppScaffoldAppDelegate.h
//  AppScaffold
//

#import <UIKit/UIKit.h>
@class MainGameStage;

@interface AppDelegate : NSObject <UIApplicationDelegate>
{
  @private 
    UIWindow *mWindow;
    SPViewController *mViewController;
}

@end
