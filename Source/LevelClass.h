//
//  LevelClass.h
//  Save the ship!
//
//  Created by Piotrek on 20.01.2013.
//
//

#import <Foundation/Foundation.h>

@class Game;
enum
{
    falaA = 0,
    falaB = 1,
    falaC = 2
    
};
typedef int TypFali;
enum gameMode
{
    gameModeSurvival,
    gameModeLevel,
    gameModeTest
};

@interface BlokFali : NSObject

@property (nonatomic) int koniecBloku;
@property (nonatomic) int dlugoscBloku;
@property TypFali typ;

@end

@interface LevelClass : NSObject

- (BlokFali *)generujElement;
+ (LevelClass *)levelClassForGameMode: (enum gameMode)mode andLevel: (int)lev;
- (void)wywalonaSkrzynia;
- (void)zeroSpeed;
- (void)configure;
- (void)startLevel;

@property (strong) NSArray *fale;
@property (nonatomic) int petla; //ile pierwszych fal powtarza się w cyklu, domyślnie fale.count
@property (nonatomic, assign) int dlugosc;
@property (nonatomic, assign) float predkoscFalowania;
@property (nonatomic, assign) float predkoscPlywania;
@property (nonatomic, assign) float poziomMorza;
@property (nonatomic, assign) enum gameMode mode;
@property (nonatomic, assign) int gameWidth;
@property (nonatomic, assign) int iloscSkrzyn;
@property (nonatomic, assign) int iloscBomb;
@property (nonatomic, assign) bool continues;
@property (nonatomic, weak) Game *game;
@property (nonatomic, assign) int levelNumber;
@property (nonatomic, assign) int runway;
@property CGPoint *bombs;
@property      int numerFali;

//Monety
@property int PakietIloscW;
@property int PakietIloscH;
@property (strong) NSArray *pakietyPozycje;
@property (nonatomic) int skrzynkiZKolem;


@end
