//
//  EndLevelInfo.h
//  Save the ship!
//
//  Created by Piotrek on 23.07.2013.
//
//

#import "SPSprite.h"
@class Game;
@interface EndLevelInfo : SPSprite
- (void)finish;
@property (weak) Game *game;
@end
