//
//  InfoBox.m
//  Save the ship!
//
//  Created by Piotrek on 04.07.2013.
//
//

#import "InfoBox.h"

@implementation InfoBox
- (id)init
{
    self = [super init];
    if (self) {
        self.title = [[SPTextField alloc] initWithWidth:100 height:100];
        self.text = [[SPTextField alloc] initWithWidth:100 height:100];
        self.text.y = -20;
        [self addChild:self.title];
        [self addChild:self.text];
        self.x = [Sparrow stage].width/2;
        self.y = [Sparrow stage].height/3.0f;
        self.pivotX = self.width/2;
        self.pivotY = self.height/2;
    }
    return self;
}
@end
