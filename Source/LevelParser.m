//
//  LevelParser.m
//  Save the ship!
//
//  Created by Piotrek on 05.06.2013.
//
//

#import "LevelParser.h"
#import "GDataXMLNode.h"

@implementation LevelParser
+ (Level *)loadLevelsForChapter:(int)chapter andLevel: (int)levNum {
    
    NSString *filePath = [self dataFilePath:FALSE forChapter:chapter];
    
    NSData *xmlData = [[NSMutableData alloc]initWithContentsOfFile:filePath];
    
    NSError *err;
    GDataXMLDocument *doc = [[GDataXMLDocument alloc]initWithData:xmlData options:0 error:&err];
    if (doc == nil) { return nil; NSLog(@"xml file is empty!");}
    NSLog(@"Loading %@", filePath);
    
    NSArray *dataArray = [doc nodesForXPath:@"//Levels/Level" error:nil];
    //    NSLog(@"Array Contents = %@", dataArray);
    NSArray *objectArray;
    GDataXMLElement *xmlElement;
    Level *lev = [[Level alloc] init];
    
    GDataXMLElement *element = [dataArray objectAtIndex:levNum];
    //    for (GDataXMLElement *element in dataArray) {
    //        NSLog(@"%@", element);
    //game width
    objectArray = [element elementsForName:@"Width"];
    if (objectArray.count>0) {
        xmlElement = [objectArray objectAtIndex:0];
        lev.gameWidth = [[xmlElement stringValue] intValue];
    }
    
    //continues
    objectArray = [element elementsForName: @"Continues"];
    if (objectArray.count>0) {
        xmlElement = [objectArray objectAtIndex:0];
        lev.continues = [[xmlElement stringValue] boolValue];
    }
    //Speed
    objectArray = [element elementsForName: @"Speed"];
    if (objectArray.count>0) {
        xmlElement = [objectArray objectAtIndex:0];
        lev.predkoscPlywania = [[xmlElement stringValue] floatValue];
    }
    objectArray = [element elementsForName: @"FalaSpeed"];
    if (objectArray.count>0) {
        xmlElement = [objectArray objectAtIndex:0];
        lev.predkoscFalowania = [[xmlElement stringValue] floatValue];
    }
    objectArray = [element elementsForName: @"Runway"];
    if (objectArray.count>0) {
        xmlElement = [objectArray objectAtIndex:0];
        lev.runway = [[xmlElement stringValue] intValue];
    }


    //Skrzynie
    objectArray = [element elementsForName:@"Skrzynie"];
    if (objectArray.count>0) {
        xmlElement = [objectArray objectAtIndex:0];
        lev.iloscSkrzyn  = [[xmlElement stringValue] intValue];
    }
    //fale
    objectArray = [element elementsForName:@"Waves"];
    if (objectArray.count>0) {
        NSArray *fale =   [(GDataXMLElement*)[objectArray objectAtIndex:0] elementsForName:@"Wave"];
        
        for (GDataXMLElement *faleElement in fale) {
            BlokFali *blok = [[BlokFali alloc]init];
            
            objectArray = [faleElement elementsForName:@"Length"];
            if (objectArray.count>0) {
                xmlElement = [objectArray objectAtIndex:0];
                blok.dlugoscBloku = [[xmlElement stringValue] intValue];
            }
            objectArray = [faleElement elementsForName:@"Height"];
            if (objectArray.count>0) {
                xmlElement = [objectArray objectAtIndex:0];
                blok.koniecBloku = [[xmlElement stringValue] intValue];
            }
            objectArray  = [faleElement elementsForName:@"Type"];
            if (objectArray.count>0) {
                xmlElement = [objectArray objectAtIndex:0];
                
                blok.typ = [[xmlElement stringValue] intValue];
            }
//            NSLog(@"Blok fali %@", blok);
            lev.fale = [lev.fale arrayByAddingObject:blok];
        }
        
    }
    //monety
    objectArray = [element elementsForName:@"Monety"];
    if (objectArray.count>0) {
        NSArray *secObjeArray  = [(GDataXMLElement*)[objectArray objectAtIndex:0] elementsForName:@"PakietIloscW"];
        if (secObjeArray.count>0) {
            xmlElement = [secObjeArray objectAtIndex:0];
            lev.PakietIloscW = [[xmlElement stringValue] intValue];
        }
        secObjeArray  = [(GDataXMLElement*)[objectArray objectAtIndex:0] elementsForName:@"PakietIloscH"];
        if (secObjeArray.count>0) {
            xmlElement = [secObjeArray objectAtIndex:0];
            lev.PakietIloscH = [[xmlElement stringValue] intValue];
        }
        
        secObjeArray = [(GDataXMLElement*)[objectArray objectAtIndex:0] elementsForName:@"Pakiet"];
        float pakx, paky;

        for (GDataXMLElement *pakiet in secObjeArray) {
            
            objectArray = [pakiet elementsForName:@"x"];
            if (objectArray.count>0) {
                xmlElement = [objectArray objectAtIndex:0];
                pakx = [[xmlElement stringValue] floatValue];
//                pakx*= (arc4random()%10)/100;
            }
            
            objectArray = [pakiet elementsForName:@"y"];
            if (objectArray.count>0) {
                xmlElement = [objectArray objectAtIndex:0];
                paky = [[xmlElement stringValue] floatValue];
//                paky*= (arc4random()%10)/100;

            }
            [lev dodajPakietMonetWX:pakx y:paky];

        }
    }
    objectArray = [element elementsForName:@"Bomby"];
    if (objectArray.count>0) {
        NSArray *secObjArray = [(GDataXMLElement*)[objectArray objectAtIndex:0] elementsForName:@"B"];
        lev.iloscBomb = secObjArray.count;
        for (int i=0; i<lev.iloscBomb; i++) {
            xmlElement = [secObjArray objectAtIndex:i];
            lev.bombs[i] = CGPointMake(xmlElement.stringValue.floatValue, 100.f);
        }
    }
    
    //    }
    
    lev.levelNumber = levNum;
    [lev configure];
    return lev;
}
+ (void)saveData:(Level *)saveData
      forChapter:(int)chapter
{
    
}

+ (NSString *)dataFilePath:(BOOL)forSave forChapter:(int)chapter {
    NSString *xmlFileName = [NSString stringWithFormat:@"Levels-Chapter%i",chapter];
    NSString *xmlFileNameWithExtension = [NSString stringWithFormat:@"%@.xml",xmlFileName];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *documentsPath = [documentsDirectory stringByAppendingPathComponent:xmlFileNameWithExtension];
    if (forSave || [[NSFileManager defaultManager] fileExistsAtPath:documentsPath]) {
        return documentsPath;
        NSLog(@"%@ opened for read/write",documentsPath);
    } else {
        NSLog(@"Created/copied in default %@",xmlFileNameWithExtension);
        return [[NSBundle mainBundle] pathForResource:xmlFileName ofType:@"xml"];
    }
    
}
@end
