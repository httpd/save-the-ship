//
//  NieboTlo.m
//  Save the ship!
//
//  Created by Piotrek on 08.10.2012.
//
//

#import "NieboTlo.h"
#import "Game.h"
@implementation NieboTlo
- (id)initWithGame: (Game *)g
{
    self = [super init];
    if (self) {
        self.game = g;
        uint  bottomColor = 0xFFFFFF;
        uint topColor = 0x008ABF;
        uint wodaTop = 0xA1D1CE;
        uint wodaColor = 0x00867E;
        SPQuad *niebo = [SPQuad quadWithWidth:[Sparrow stage].width height:[Sparrow stage].height];
        [niebo setColor:topColor    ofVertex:0];
        [niebo setColor:topColor    ofVertex:1];
        [niebo setColor:bottomColor ofVertex:2];
        [niebo setColor:bottomColor ofVertex:3];
        [self addChild:niebo];
        
        
        SXParticleSystem *system;
        if (self.game.level.levelNumber==0 ) {
            system = [SXParticleSystem particleSystemWithContentsOfFile:@"chmurki.pex"];

        }else if(self.game.level.levelNumber==2)
        {
            system = [SXParticleSystem particleSystemWithContentsOfFile:@"chmurki-arktyka.pex"];
        }
        else
        {
            system = [SXParticleSystem particleSystemWithContentsOfFile:@"chmurki_burza.pex"];
        }
        system.x = [Sparrow stage].width*2;
        system.y = 60;
        system.lifespan = 50;
        [self.game.juggler addObject:system];
        [system start];
        [system advanceTime:system.lifespan];

//        SPImage *horizon= [SPImage imageWithTexture:[self.game.atlas textureByName:@"blyski"]];
//        horizon.y+=50;
//        [self addChild:horizon];
        
        SPQuad *woda2 = [SPQuad quadWithWidth:[Sparrow stage].width height:([Sparrow stage].height/2-20)];
        woda2.y = [Sparrow stage].height/2+20;
        woda2.color = wodaColor;
        [self addChild:woda2];
        

        
        SPQuad *woda = [SPQuad quadWithWidth:[Sparrow stage].width height:([Sparrow stage].height/2-20)/2];
        woda.y = [Sparrow stage].height/2+20;
        
        [woda setColor:wodaTop ofVertex:0];
        [woda setColor:wodaTop ofVertex:1];
        [woda setColor:wodaColor ofVertex:2];
        [woda setColor:wodaColor ofVertex:3];
        [self addChild:woda];
        if (self.game.level.levelNumber==2) {
            SPImage *img=[SPImage imageWithContentsOfFile:@"Arktyka-1.png"];
            [self addChild:img];
        }

        
        // cast the texture to SPGLTexture (only possible with power-of-2 textures)
        SPGLTexture *noiseTexture = (SPGLTexture *)[SPTexture textureWithContentsOfFile:@"filmGrain.png"];
        noiseTexture.repeat = YES; // let it repeat
        
        // create SPImage with that texture
        SPImage *noiseLayer = [SPImage imageWithTexture:noiseTexture];
        noiseLayer.width = Sparrow.stage.width;
        noiseLayer.height = Sparrow.stage.height;
        noiseLayer.alpha = 0.03;
        noiseLayer.touchable = NO;
        [self addChild:noiseLayer];
        
        // this is the important part: changing the texture coordinates
        // so that the texture is repeated.
        float xRepeat = Sparrow.stage.width/noiseTexture.width;
        float yRepeat = Sparrow.stage.height/noiseTexture.height;
        [noiseLayer setTexCoords:[SPPoint pointWithX:xRepeat y:0.0f] ofVertex:1];
        [noiseLayer setTexCoords:[SPPoint pointWithX:0.0f y:yRepeat] ofVertex:2];
        [noiseLayer setTexCoords:[SPPoint pointWithX:xRepeat y:yRepeat] ofVertex:3];
        
        [self addChild:system];

//        [self flatten];
        
//        SPQuad *quad = [SPQuad quadWithWidth:self.width height:self.height color:<#(uint)#>]
        

    }
    return self;
}
@end
