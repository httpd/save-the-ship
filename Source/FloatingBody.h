//
//  FloatingBody.h
//  Save the ship!
//
//  Created by Piotrek on 15.12.2012.
//
//

#import <Foundation/Foundation.h>
#import "Box2D.h"
@class Game;
@interface FloatingBody : SPSprite
{
    b2World *world;
@public
    b2Body *body;
}
- (id)initWithWorld: (b2World *)_world;
- (void)wyjsciePozaZakres;

@property bool dragable;
@property bool destroyed;
@property bool submerged;
@property bool draging;
@property (nonatomic, assign) long long *offset;
@property (nonatomic, weak) NSMutableArray *floatingBodies;
@property (nonatomic, weak) Game *game;
@property     int czas;
- (void)testCatchAnimation;
@end
