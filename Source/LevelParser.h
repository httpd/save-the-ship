//
//  LevelParser.h
//  Save the ship!
//
//  Created by Piotrek on 05.06.2013.
//
//

#import <Foundation/Foundation.h>
#import "Level.h"
@interface LevelParser : NSObject

+ (Level *)loadLevelsForChapter:(int)chapter andLevel: (int)levNum;
+ (void)saveData:(Level *)saveData
      forChapter:(int)chapter;
@end
