//1
attribute vec4 a_position;
attribute vec2 a_texCoord;
 
//2
uniform mat4 m_matrix;
 
//3
#ifdef GL_ES
varying mediump vec2 v_texCoord;
#else
varying vec2 v_texCoord;
#endif

//4
void main()
{
//5
  gl_Position = a_position * m_matrix;
//6
  v_texCoord = a_texCoord;
}